/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
	1.英里单位显示mile 																			- 2017-01-21
	2.手动控制大灯后，屏蔽自动大灯功能 											- 2017-01-21
	3.设置模式下自动退出时间：由10秒更改为1分钟							- 2017-01-21
	4.功率圆弧，内圆半径改为85															- 2017-01-21
	5.轮径设置为27时，按27.5进行计算												- 2017-01-21
	6.报错信息显示界面更改																	- 2017-01-21
	7.菜单增加一页index																			- 2017-01-21
	8.完善EMC模式，更正连续20秒的条件												- 2017-02-18
	9.总里程大于16770000时，清零														- 2017-02-18
	10.更改上位机通信异常的问题，（通信超时后，不清rxbuf）	- 2017-02-21
	11.八方生产序列号长度最长改为40位（原来为15位）					- 2017-03-02
	12.增加错误代码范围限制																	- 2017-03-03
	13.增加工程模式，该模式下，不报错，错误代码不记忆				- 2017-03-10
	14.报错时也能进入设置菜单																- 2017-03-22
	15.更改退出设置菜单后，SOC偶尔显示为0%的问题（通讯超时）- 2017-03-22
	16.更改进入PC模式背光未点亮的问题												- 2017-03-22
	17.错误代码重新分配存储空间															- 2017-03-22
	18.更改开机密码输入界面背光未点亮的问题									- 2017-03-24
	19.默认开机密码改为0 0 0 0 （原为0512）									- 2017-03-24
	20.更改开机动画，及背景颜色（绿色——微信图标绿）					- 2017-03-24
	21.自动退出设置界面的时间由1min改为20秒									- 2017-03-24
	22.校准实时时钟																					- 2017-03-28
	23.更正LOGO下方小字。Groal->Goal												-	2017-04-05
	24.增加开机LOGO的ULTRA模式															-	2017-04-05
	25.修改USB图标有时被盖住一部分的BUG											- 2017-04-07
	26.修改限速值显示计算过程数据溢出的BUG									- 2017-04-10
	27.增加光感灵敏可调功能																	- 2017-04-14
	28.限速、轮径可调																				- 2017-04-15
	29.增加3秒内收不到数据，串口复位功能										- 2017-04-29
	30.增加密码功能逻辑																			- 2017-05-02
	31.增加无ECO/SPORT切换模式的程序												- 2017-05-03
	32.增加档位数设置功能。可设置最大档位数3.5.9						- 2017-05-03
	33.密码功能，输入密码时如果密码未输入显示 ‘-’						- 2017-05-03
	34.输入密码时，‘-’不能作为合法密码。按i键，密码不移位		- 2017-05-04
	35.range、卡路里读取不到数据时，显示‘0’。原来为‘-’			- 2017-05-05
	36.开机10秒之内如果未读取到剩余里程、卡路里数据，则不
		 显示这两项，同时串口不再发送这两项的读取命令					- 2017-05-10
	37.刷机后客户号、车架号、生产序列号清零									- 2017-05-17
	38.密码设置，提示成功后立即保存设置											- 2017-05-17
	39.速度显示大于100时，显示99.9													- 2017-05-16
	40.增加速度表盘自适应功能，分30、60、90三个级别					- 2017-05-16
	41.增加对客户号、车架号、生产序列号的长度限制						- 2017-06-10
	42.删除PVL电压分档存储数据															- 2017-06-13
	43.增加ODO备份数据，纠错机制														- 2017-06-13
	**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**--**
	Bin Head  01 45 02 00 00 00 00 00 00 00 00 00 00 00 00 00 
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "stm32f2xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "wwdg.h"
#include "gpio.h"
//#include "Config.h"
//#include "Config(0420).h"
//#include "Config(713NZxx099999999).h"//彩色闪电标 调试
#include "Config(713Nxxx0B).h"
//#include "Config(713-1B77).h"//LUNA标，开机默认 0 档（李涛要求调试用）
/* USER CODE BEGIN Includes */
#include "delay.h"
#include "lcd.h"
#include "sys.h"

#ifdef ULTRA_ON
#include "ULTRA.h"
#endif


/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define THE_CODE_Length 8141

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
void Send_PAS_Value(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
RTC_TimeTypeDef stimestructure;
uint8_t RX_Flag , USART1_RXTime_OUT;
uint8_t Rx_Len , RX_NUM;
uint8_t SYS_Unit_S369_Flag;
uint16_t Shut_SYS_Delay ;
uint16_t USB_Check_Delay , USB_Relase_Delay;

uint32_t ADC_Value[150];

uint16_t LIGHT_ON_SET = 4000;
uint16_t LIGHT_OFF_SET = 3980;

uint16_t LIGHT_SW_LEVE[5] = 
{
					3900,
					3950,
					4000,
					4020,
					4040
};

uint8_t AUTO_LIGHT_Sensitivity;

uint16_t AL_Standard_Value;

//uint32_t ODO_Test;

uint8_t  Timer_Hour , Timer_Minute , Timer_Second , TIME_Set_Mask;
uint8_t  Timer_Second_Delay;

u8 Sys_New_Born_Flag;//系统刚烧完程序，参数需要初始化。初始化完成后，会将0XAA写入 数据存储区的 第一个字节 。所以，当读出的数据为0xaa时，则不需要初始化参数
u8 LOGO_Dis_Flag;

/*配置表
1：公制/英制  单位选择
2：背光亮度 0-4 五级选择
3：自动关机时间 OFF、1-9分钟
4：场景 数字/模拟
5：电压/百分比
6：清零单次里程
7：轮径
8：电压级别  24  36   48--------作废  暂留
9：限速值
10:光敏灵敏度
*/
u8 Config_Temp[10]={0,5,5,0,1,0,28,0,25,3};
u8 Error_Code_Tab[11]={0,0,0,0,0,0,0,0,0,0};
u8 S_Num_Err;
u8 Bat_Message[26]={
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0};
u8 Bat_Valtages[42]={//支持最大20串
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0,
	0,0,0,0,0,0,0,0
};
u8 Bat_Index_Pages=0 , Bat_Index_Page_Reload=0 , Battey_Index_Waiting=0 , Bat_Sirl_Num;

u8 WALK_EXIT_DELAY;

u8 Send_Lenth=0;
u8 Send_Mode=1,Send_Mode_S=1,Send_Mode_Deep=0;
u8 UART_Send_Settings_Delay;
u8 UART_Send_Settings_Flag;
u8 UART_Send_Allowed_Flag;
u8 Control_ANS_Flag;
u8 Send_Uart_Delay,Send_UART_Settings_Delay;
u8 Call_Reply_Flag,Call_Reply_Mask;

u8 Moto_Working_Flag;

u16 Time_For_BF_ShutWithClear;
uint16_t AVG_Buf = 0;

u8 Key_Scan_Delay;
u16 Wheel_T;// , S_Wheel_T;
u8 PAS_Value,PAS_Value_Buffer,PAS_Value_NG,PAS_Value_NNG,PAS_Top_Vf,PAS_NG_Delay = 0;

u8 BF_PAS_STab_9D[9]={1,11,12,13,2,21,22,23,3};
u8 BF_PAS_STab_5D[5]={11,13,21,23,3};
u8 BF_PAS_STab_3D[3]={12,2,3};
u8 PAS_Temp6;
u8 *PAS_Tab_Set;
u8 PAS_Send_Now_Mask;//PAS发生变化，立即发送PAS

u8 Trip_Record_Save_Delay;

u8 Sys_Code_Tab[4]={0,0,0,0};
u8 User_Input_Code_Tab[4]={0,0,0,0};
u8 User_Input_Code_Buf[4]={0,0,0,0};
u8 Code_Input_Flag;

u16 Wheel_Perimeter;
u8  Wheel_Dia_Mark;//轮径代码
u32 KM_Speed_Base_Value,MH_Speed_Base_Value,Speed_Base_Value;
u16 Speed_Buf,D_Speed00,D_Speed,CCS_Speed,CCS_Speed_Stro;
u8 Speed_ReLoad_Delay;//,Speed_ReLoad_Delay_Top;

u8 Walk_PWM_Temp,Walk_Add_Delay;

u16 SYS_Voltage , SYS_Voltage_Fr , SYS_BAT_Flash;
u16 SYS_Voltage_Start,SYS_Voltage_Dec;
u8  Voltage_Check_SS,SYS_Voltage_DelayS;
u8  Voltage_DEC_Delay,Voltage_DEC_Base;
u8  LOW_Voltage_Flag;

u16 Light_ADCIN;

//u8  MAX_Limit_Speed;
u8  DL_Delay_Time;//电量延时 1-3S  2-6S  3-12S
u8  C_Current , Battery_SOC , Battery_SOC_FR , Battery_SOC_Start , Battery_SOC_Flit;
u8  V_Sys_Worked;
u16 V_Standard_Value,V_In_Value;
u16 Motor_Power_CC,Motor_Power_CC_Br;

u8  D500mS_Count;
u8  Send_Data[10];//发送缓存，最大十个字节
u8  Send_States_Flag;//数据发送状态，5S协议下定义：0-未启动发送，1-发送系统参数设置命令，2-发送运行数据

u16  Setting_Value_Temp,Setting_Value_Top_Limit,Setting_Value_Bottom_Limit;

u8 SYS_Pus_FR,SYS_Pus_SCN,SYS_Auto_OFF,SYS_P_V_Seclect,SYS_CUR_Limit_Value,PAS_CP_CGN;
u8 Speed_Limte_Code,Speed_Limte_BUF,Speed_Limte_MPH;

u8 Speed_Over_Limte_Check,Speed_Over_Limte_Flag,PAS_Value_Dec;

u8  Restore_Settings_Flag;
u8  Avg_JS_Flag , AVG_Dec_Delay;

u8 NoCommunication_OverStand;

u32 Sys_Auto_ShutUp_Delay;

uint8_t  BF_Gust_Sirl[26];
uint8_t  BF_Product_SN[41];
uint8_t  BF_Frame_SN[16];

uint8_t  BF_SWHW_Ver_Tab[16];//软件版本号
//0X10,0X0D,'D','P','C','1','8','P','1','0','1','0','2','.','0',0XFF};
uint8_t  BF_HW_Ver_Tab[15];//硬件版本号
//0X17,0X0C,'D','P',' ','C','1','8','.','U',' ','1','.','1',0XB6};
uint8_t  BF_Model_Tab_Temp[15];//仪表型号
//0X11,0X0A,0X0C,'D','P',' ','C','1','8','.','U',' ','1','.','0',0XB9
//0X11,0X0A,0X0B,'D','P',' ','C','1','8','.','U','A','R','T',0XF0
//};
uint8_t  G_Tab_Temp[50];

struct//特殊模式相关标识EMC
{
	u16 Start_Delay:16;//开机3分钟计时
	u16 Check_Delay:16;//速度稳定计时
	u8  EN_Flag:1;//EMC模式标识
	u16 EMC_Sped_Buf:16;//EMC下速度记忆
	u8  EMC_Check_OK:1;//当前条件满足EMC测试条件
	u16  EMC_Exit_Delay:8;//退出检测
	u8  EMC_Exit_Delay2:8;//退出检测
}EMC_TEST;

struct//显示相关标识
{
	u8 Bat_ReLoad_Flag:1;//电池电量显示刷新
	u8 Bat_ShutDis_Flag:1;//电池符号关闭显示标识  用于 欠压闪烁
	u8 Spd_ReLoad_Flag:1;//速度显示刷新
	u8 PCC_ReLoad_Flag:1;//功率显示刷新
	u8 PAS_ReLoad_Flag:1;
	u8 ERR_ReLoad_Flag:1;
	u8 TIME_ReLoad_Flag:1;
	u8 Light_ReLoad_Flag:1;
	u8 USB_ReLoad_Flag:1;
	u8 WALK_ReLoad_Flag:1;
	u8 RANGE_DIS_EN:1;
	u8 CALORIES_DIS_EN:1;
	u8 E_S_SW_Flag:1;
	u8 R_S_Flag:1;
	u8 C_S_Flag:1;
	u8 R_DIS_SDelay:8;//开机20秒内有答复
	u8 C_DIS_SDelay:8;//开机20秒内有答复
}LCD_DisPlay;

struct//设置模式相关变量、标识
{
	unsigned char i_Key_IN:1;//    i键双击标识
	unsigned char i_Key_ON:1;//    i键单击标识
	unsigned char i_Key_DL:8;//    i键双击延时
	unsigned char Mode:8;//      菜单级别
	unsigned char Start_Code_Flag:1;//开机密码标识
	//unsigned char Level:8;//     子菜单序号
	unsigned char Selected:8;//  当前子项
	unsigned char Chosen:8;  //  选定子项
	unsigned char View_S:8;  //  选定子项
	unsigned char In_Flag:1;//   设置模式标识 
	unsigned char Exit_Flag:1;//   退出设置模式标识 
	unsigned char UpDat_Flag:1;//界面及初始化参数更新标识
	unsigned char Card_ReLoad_Flag:1;
	unsigned char Setting_ReLoad_Mask:1;//界面及初始化参数更新标识
	unsigned char Set_OK_View:1;//设置OK界面
	unsigned char Set_OK_View_Delay:8;//显示OK延时计数
	unsigned char SOC_Rev_Flag:1;//接收到控制器返回的SOC值了，如果此标识为0，则使用采集到的电压值显示电量
	unsigned char Restor_Now_Flag:1;//恢复出厂设置界面
	unsigned char Clrear_Trip_Flag:1;//清零单次里程操作
	unsigned char Save_PassWord_Flag:1;//清零单次里程操作
	unsigned char PassWord_Changed_Flag:1;
	unsigned char Error_Code_Clear_Flag:1;//清理错误标识
	unsigned char Time_Need_To_Save_Flag:1;
	unsigned char PW_Last_States:1;//输入密码界面开启
	unsigned char Password_Set_States:8;//密码界面流程标识
	unsigned char PW_Set_MSG_Flag:8;
	unsigned char PW_Set_MSG_Delay:8;
	unsigned char Real_Time_Edit_Flag:1;
}Setting;

struct//工作模式、状态标识
{
  unsigned char Interface_ReLoad_Mask:1;//界面刷新标识
  unsigned char Working_Mode:8;//0-5  5种界面显示
  unsigned char P_Walk_Flag:1;//助力推行模式
  unsigned char Speed_Unit_GY_Flag:1;//速度单位转换标识
  unsigned char Power_Unit_PI_Flag:1;//功率单位转换标识
  unsigned char Light_Switch_Flag:1;//大灯开启关闭标识
  unsigned char BLK_Light_Level:8;//背光亮度级别
  unsigned char Current_Err_CODE:8;//错误代码
  unsigned char Current_Err_CODE_Bk:8;//错误代码
  unsigned char SYS_Err_Occurred_Flag:1;//错误产生标识
  unsigned char Moving_State_Chg:1;//运动状态发生改变
  unsigned char SYS_Power_Just_ON:1;//系统初始上电
  unsigned char SYS_Power_States_Flag:1;//系统初始上电标识
  unsigned char SYS_Start_ON_Flag:1;//完成开机标识
  unsigned int  Current_Speed:16;//当前车速
  unsigned char SCA_Leve_Setting:8;//助力档位
  unsigned char SYS_Setting_OverFlow:8;//系统设置超时计数
  unsigned char NoCommunication_OverFlow:8;//通讯超时计数
  unsigned char SYS_CodeIN_Setting:8;//密码设置
  unsigned char SYS_Settint_Reset_Flag:1;
  unsigned char Call_IN_From_PC_Flag:1;//PC端呼叫标识
  unsigned char SYS_Start_PassWord_EnAble:1;//需要开机密码输入标识
	unsigned char SYS_Start_PassWord_SBuf:1;//需要开机密码输入标识
  unsigned char Bike_Moving_Flag:1;//速度为0标识
  unsigned char MAX_SPD_Reset:1;//存储最大速度标识
  unsigned char ShutDown_Flag:1;//关机命令标识
  unsigned char See_Volatge_Flag:1;//计算电压命令
  unsigned char Save_Datas_Flag:1;//保存数据命令
  unsigned char USB_State_Flag:1;
  unsigned char USB_Sleep_Flag:1;
  unsigned char Light_Sensing_Flag:1;//光感状态标识
  unsigned char Light_Sense_Filter:8;//光感状态滤波
  unsigned char Light_Sensing_EN:1;//光感控制有效标识，开机有效，手动操作后失效
	unsigned char Engineering_Mode_MASK:1;//工程模式标识，该模式下，不记忆错误代码
}Work_Status;

struct//里程计算相关变量
{
  u16  Base_Time_Count_Delay:16;
  u16  Base_Time_Count_NUM:16;
  u32  Base_mm_Count:32;//单位mm
  u16  Trip_Time_Delay:16;
}Mileage_Variable;

struct//里程数据
{
  u32  Total_Trap_Record:32;//0-999  总里程
  u32  Total_Trap_Record_Mile:32;//0-999  总里程(英制)
  u16  Service_Milage:16; //售后里程记录
  u16  Service_Milage_ADD:8; //售后里程记录
  u16  Single_Trap_At_Once:16; //单次里程记录
  u16  Single_Trap_At_Once_Mile:16;//单次里程记录(英制)
  u16  Timer_Of_This_Trap:16;   //运动时间记录
	u16  Second_Of_This_Trap:16;
  u16  MAX_Speed_In_This_Trap:16;//最高速度
  u16  MAX_Speed_In_This_Trap_Mile:16;//最高速度(英制)
  u16  AVG_Speed_In_This_Trap:16;//平均速度
  u16  AVG_Speed_In_This_Trap_Mile:16;//平均速度(英制)
  u16  Trap_Range:16;//剩余里程
  u16  Trap_Range_Fr:16;//剩余里程
  u16  S_Calorie:16;//卡路里
  u16  S_Calorie_Fr:16;//卡路里
}Sport_Recording;

struct//串口数据收发相关标识
{
	u8 Need_Rev_Data_Mask:1;//标识需要串口回复数据
	u8 RevBack_Time_Out:8;//等待回复超时计时
	u8 RevBack_Time_Out_Flag:1;//接收超时标识
	u8 Just_Send_Flag:1;//数据发送标识
	u8 Send_Time_Delay:8;//数据发送最小间隔
	u8 Send_Scenes_Flag:1;//发送工作模式
	u8 Send_Scenes_Delay:8;//发送延时、间隔
	u8 Send_Scenes_TimeOut:8;//发送超时命令
	u8 REV_Back_OUT_Time:8;//接收超时计时，超时后复位串口
}MY_USART;
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
#ifdef Client_LOGO_EN//显示客户LOGO，根据图片长款进行显示，固定升起动画
void Dis_LOGO(uint8_t Sf)
{
	uint16_t i,j,t,S_x,S_y;
	
	S_x = (320-LOGO_W)/2;
	S_y = ((480-LOGO_H)/2) + LOGO_H - 50;
//	S_y-=50;

	for(t=0;t<50;t++)
	{
		for(i=0;i<LOGO_H;i++)
		{
			LCD_SetXY(S_x , S_y+t-i , LOGO_W);
			for(j=0;j<LOGO_W;j++)
				LCD_WriteData(((Client_LOGO[(LOGO_W*i*2) + (2*j)+1]<<8) | Client_LOGO[(LOGO_W*i*2) + (2*j)]));
		}
		LCD_SetXY(S_x , S_y+t-LOGO_H , LOGO_W);
		for(j=0;j<LOGO_W;j++)
			LCD_WriteData(Def_BGC);
//		if(t==0)
//			delay_ms(5000);
		delay_ms(10);
	}
	delay_ms(1000);
}
#else
void Dis_LOGO(uint8_t Sf)
{
	uint16_t i,j,t;
	
	#ifdef FLX_LOGO
	uint8_t  Bs , k;
//	uint16_t F_CLoro;
//	F_CLoro = White1;
	for(t=0;t<50;t++)
	{
		for(i=0;i<97;i++)
		{
			LCD_SetXY(28,(238+t-i),264);
			for(j=0;j<33;j++)
			{
				Bs = Image_FLX[i*33+j];
				for(k=0;k<8;k++)
				{
					if(Bs&0x01)
						LCD_WriteData(White1);
					else
						LCD_WriteData(Black);
					Bs>>=1;
				}
			}
		}
//	LCD_SetXY(28,(208+(2*t)-i));
//	LCD_WriteReg(0x0022);
//	for(j=0;j<264;j++)
//		LCD_WriteData(Black);
		delay_us(100);
	}
	delay_ms(300);
	#else
	#ifdef EV_BIKE_LOGO
	uint8_t  Bs , k , l;
	uint16_t F_CLoro;
	for(t=0;t<50;t++)
	{
		for(i=0;i<224;i++)
		{
			LCD_SetXY(60,(302+t-i),200);
			for(j=0;j<25;j++)
			{
				Bs = Image_EVBike[i*25+j];
				for(k=0;k<8;k++)
				{
					l=j*8+k;
					if((l<56 && i<82)||(l<75 && i>181)||(l>141 && l<176 && i>112 && i<148))
						F_CLoro = Cyans;
					else
						F_CLoro = White1;
					if(Bs&0x01)
						LCD_WriteData(F_CLoro);
					else
						LCD_WriteData(Black);
					Bs>>=1;
				}
			}
		}
		LCD_SetXY(60,(302+t-i),201);
		for(j=0;j<201;j++)
			LCD_WriteData(Black);
	}
	#else
	u16 Dat;
	for(i=0;i<51;i++)
	{
		LCD_SetXY(70,(260-i),180);
		for(j=0;j<180;j++)
		{
			Dat=((Image_LOGO1[2*((i*180)+j)+1]<<8) | Image_LOGO1[2*((i*180)+j)]);
			LCD_WriteData(~Dat);
		}
	}
	delay_ms(50);
	#ifdef ULTRA_ON
	for(j=0;j<28;j++)
	{
			for(i=0;i<23;i++)
			{
					LCD_SetXY(50+(j*8),(200-i),8);
					Dat=ULTRA_WORDS1[i*28+j];
					for(t=0;t<8;t++)
					{
							if(Dat&0x01)
								LCD_WriteData(White);
							else
								LCD_WriteData(WGreen);
							Dat>>=1;
					}
			}
	}
	delay_ms(25);
	for(j=0;j<38;j++)
	{
			for(i=0;i<20;i++)
			{
					LCD_SetXY(10+(j*8),(165-i),8);
					Dat=ULTRA_WORDS2[i*38+j];
					for(t=0;t<8;t++)
					{
							if(Dat&0x01)
								LCD_WriteData(White);
							else
								LCD_WriteData(WGreen);
							Dat>>=1;
					}
			}
			delay_ms(25);
	}
	delay_ms(300);
	#else
	for(j=0;j<24;j++)
	{
			for(i=0;i<23;i++)
			{
					LCD_SetXY(65+(j*8),(195-i),8);
					Dat=LOGO_Words[i*24+j];
					for(t=0;t<8;t++)
					{
							if(Dat&0x01)
								LCD_WriteData(White);
							else
								LCD_WriteData(WGreen);
							Dat>>=1;
					}
			}
			delay_ms(25);
	}
	#endif
	#endif
	delay_ms(800);
	#endif
}
#endif
/*开机MCU初始化*/
void Start_MCU(void)
{/* MCU Configuration----------------------------------------------------------*/
  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();
	SystemClock_Config();
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_TIM2_Init();
	MX_TIM3_Init();
	MX_ADC1_Init();
	MX_RTC_Init();
	MX_WWDG_Init();
	delay_init(120);
	USART1_RXTime_OUT = 2;
	MX_USART1_UART_Init(1200);
	Int_Uart_DMA_Receive();//开启串口接收
	HAL_ADC_Start_DMA(&hadc1, (uint32_t*)&ADC_Value, 150);/*-1- Start the DMA - ADC*/
	HAL_TIM_Base_Start_IT(&htim2);/*-2- Start the TIMER*/
	HAL_WWDG_Start_IT(&hwwdg);	/*-3- Start the WWDG*/
	delay_ms(5);
}
/*开机参数初始化*/
void Initialization_System_Start(void)
{
	ADC_Value[147] = 0;
	Work_Status.SYS_Start_ON_Flag = 0;
	Work_Status.SYS_Power_States_Flag = 0;
	Work_Status.SYS_Power_Just_ON=1;
	Sport_Recording.Trap_Range=0;
	Work_Status.Interface_ReLoad_Mask=0;
	PAS_Value=DEFAULT_START_PAS;
	Setting.Start_Code_Flag = 0;
	Work_Status.USB_Sleep_Flag = 0;
	V_Sys_Worked=SYSTEM_DEFAULT_VOLTAGE;
	Setting.SOC_Rev_Flag = 0;
	Work_Status.Call_IN_From_PC_Flag=0;
	Send_States_Flag=0;
	Mileage_Variable.Base_Time_Count_Delay=60000;
	Sys_Auto_ShutUp_Delay=0;
	Restore_Settings_Flag=0;
	Sport_Recording.S_Calorie=0;
	if(Sport_Recording.Timer_Of_This_Trap>0 || Sport_Recording.Second_Of_This_Trap>0)
		Sport_Recording.AVG_Speed_In_This_Trap=(Sport_Recording.Single_Trap_At_Once*3600)/((Sport_Recording.Timer_Of_This_Trap*60) + Timer_Second + Sport_Recording.Second_Of_This_Trap);
	else
		Sport_Recording.AVG_Speed_In_This_Trap=0;
	PAS_Temp6=Work_Status.SCA_Leve_Setting/2*2+3;
	if(PAS_Temp6==3)
		PAS_Tab_Set=BF_PAS_STab_3D;
	if(PAS_Temp6==5)
		PAS_Tab_Set=BF_PAS_STab_5D;
	if(PAS_Temp6==7)
		PAS_Tab_Set=BF_PAS_STab_9D;
	PAS_Top_Vf=Work_Status.SCA_Leve_Setting/2*2+3;
	if(PAS_Top_Vf==7)
		PAS_Top_Vf=9;
	Time_For_BF_ShutWithClear = 0;
	
	if(DL_Delay_Time==3)
		SYS_Voltage_DelayS=12;
	else
		SYS_Voltage_DelayS=DL_Delay_Time*3;
	
	SYS_Voltage_Start=0;
	Voltage_DEC_Base=50;
	if(Wheel_Dia_Mark==27)
		Wheel_Perimeter = 2193;
	else
		Wheel_Perimeter = (u32)(Wheel_Dia_Mark*79756/1000);
	KM_Speed_Base_Value=Wheel_Perimeter*36;
	MH_Speed_Base_Value=Wheel_Perimeter*360/16;
	
	if(Work_Status.Speed_Unit_GY_Flag)
		Speed_Base_Value=MH_Speed_Base_Value;
	else
		Speed_Base_Value=KM_Speed_Base_Value;
	HAL_RTC_GetTime(&hrtc, &stimestructure, RTC_FORMAT_BIN);//从RTC获取时钟
	Timer_Hour = stimestructure.Hours;
	Timer_Minute = stimestructure.Minutes;
	Timer_Second = stimestructure.Seconds;
	
	EMC_TEST.Check_Delay = 0;
	NoCommunication_OverStand = Err_30_Time_Delay;
	Work_Status.Light_Sensing_Flag = 0;
	Setting.In_Flag=0;
	Setting.Mode=0;
	Rx_Len = 0;
	Rx_Cnt = 0;
	Rx_Time_Over = 0;
	RX_Flag = 0;	
	Battery_SOC_Flit = 0;
	LCD_DisPlay.RANGE_DIS_EN = 1;
	LCD_DisPlay.CALORIES_DIS_EN = 1;
	LCD_DisPlay.R_S_Flag = 1;
	LCD_DisPlay.C_S_Flag = 1;
	
	if(AUTO_LIGHT_Sensitivity!=0)
	{
		Work_Status.Light_Sensing_EN = 1;
		if(AUTO_LIGHT_Sensitivity==5)
				LIGHT_ON_SET = AL_Standard_Value-160;
		else if(AUTO_LIGHT_Sensitivity==4)
				LIGHT_ON_SET = AL_Standard_Value-120;
		else if(AUTO_LIGHT_Sensitivity==3)
				LIGHT_ON_SET = AL_Standard_Value-80;
		else if(AUTO_LIGHT_Sensitivity==2)
				LIGHT_ON_SET = AL_Standard_Value-60;
		else if(AUTO_LIGHT_Sensitivity==1)
				LIGHT_ON_SET = AL_Standard_Value-40;
		LIGHT_OFF_SET = (LIGHT_ON_SET-20);
	}
	else
		Work_Status.Light_Sensing_EN = 0;
}
/*系统参数初始化*/
void Initialise_SYS_Parameter_Value(void)
{u8 i;
	/*开机密码不使能 ， 默认密码 1212*/
	Sys_Code_Tab[0]=0;
	Sys_Code_Tab[1]=0;
	Sys_Code_Tab[2]=0;
	Sys_Code_Tab[3]=0;
	Work_Status.SYS_Start_PassWord_EnAble=0;
	/*助力档位 默认 0-5*/
	Work_Status.SCA_Leve_Setting=DEFAULT_SCA_LEVEL;
	/*背光亮度*/
	Work_Status.BLK_Light_Level=DEFAULT_BACK_LIGHT;
	/*轮径代码*/
	Wheel_Dia_Mark = DEFAULT_WHEEL_DIAMETER;
	/*限流值*/
	SYS_CUR_Limit_Value=DEFAULT_CURRENT_LIMTE;
	/*限速值*/
	Speed_Limte_Code=DEFAULT_SPEED_LIMTE-10;//实际限速值 = 15+10
	/*速度传感器磁钢数*/
//	SYS_Pus_CPn=2;
	/*功率、电流显示切换*/
	Work_Status.Power_Unit_PI_Flag = DEFAULT_PI_DISPLAY;
	/*场景设置*/
	Scenes_Mode_Flag = 0;
	/*自动关机时间设定*/
	SYS_Auto_OFF=DEFAULT_AUTO_OFF_TIME;
	/*助力传感器方向*/
	SYS_Pus_FR=0;
	/*助力传感器灵敏度  -------->更改为光感灵敏度  */
	SYS_Pus_SCN=2;
	/*助力传感器磁盘磁钢数*/
	PAS_CP_CGN=12;
	/*电压显示设置：电压值 / 百分比*/
	SYS_P_V_Seclect=1;
	/*电量延时*/
	DL_Delay_Time=1;
	/*最大限速值*/
//	MAX_Limit_Speed=40;
	/*公制、英制切换标识*/
	Work_Status.Speed_Unit_GY_Flag=DEFAULT_UNIT_MASK;
	/*电压基准值*/
	if((V_Standard_Value < 100)||(V_Standard_Value > 1000))
		V_Standard_Value = 234;
	/*光感基准值*/
	if((AL_Standard_Value > 4094)||(AL_Standard_Value<3000))
		AL_Standard_Value = 4000;
	/*光感灵敏度*/
	AUTO_LIGHT_Sensitivity = DEFAULT_AUTO_LIGHT_LEVEL;
	
	Sport_Recording.Single_Trap_At_Once=0;
	Sport_Recording.Timer_Of_This_Trap=0;
	Sport_Recording.MAX_Speed_In_This_Trap=0;
	Sport_Recording.AVG_Speed_In_This_Trap = 0;
	Sport_Recording.Service_Milage = 0;
	/*清理错误代码*/
	for(i=0;i<11;i++)
		Error_Code_Tab[i]=0;
//	S_Num_Err = 0;
}

/*八方 -> 客户号读写*/
void Read_Save_Gust_Sirl(u8 States)
{
	if(States)//读客户号
	{
		AT24CXX_Read(80,BF_Gust_Sirl,26);
	}
	else
	{
		AT24CXX_Write(80,BF_Gust_Sirl,26);
		delay_ms(5);
	}
}
/*八方 -> 车架号读写*/
void Read_Save_Frame_SN(u8 States)
{
	if(States)//读车架号
	{
		AT24CXX_Read(124,BF_Frame_SN,16);
	}
	else
	{
		AT24CXX_Write(124,BF_Frame_SN,16);
		delay_ms(5);
	}
}
/*八方 -> 生产序列号读写*/
void Read_Save_Product_SN(u8 States)
{
	if(States)//读生产序列号
	{
		AT24CXX_Read(140,BF_Product_SN,41);
	}
	else//写生产序列号
	{
		AT24CXX_Write(140,BF_Product_SN,41);
		delay_ms(5);
	}
}
/*八方特有参数（车架号、客户号、产品序列号）初始化*/
void BF_SYS_Parameter_INT(void)
{
	u8 i;
	for(i=0;i<26;i++)
		BF_Gust_Sirl[i]=0;
	Read_Save_Gust_Sirl(0);
	for(i=0;i<16;i++)
		BF_Frame_SN[i]=0;
	Read_Save_Frame_SN(0);
	for(i=0;i<41;i++)
		BF_Product_SN[i]=0;
	Read_Save_Product_SN(0);
}
/*八方 -> 错误代码读写*/
void Read_Save_Err_CODE(u8 States)
{
	if(States)//错误代码
	{
		AT24CXX_Read(106,Error_Code_Tab,11);
	}
	else//写误代码
	{
		AT24CXX_Write(106,Error_Code_Tab,11);
		delay_ms(5);
	}
}

/*EEPROM 数据 读写 —— 单次里程/时间*/
void Read_Save_Single_Trap(u8 States)
{
	if(States)//读单次里程数据
	{
		Sport_Recording.Single_Trap_At_Once=AT24CXX_ReadLenByte(25,2);
		Sport_Recording.Timer_Of_This_Trap=AT24CXX_ReadLenByte(27,2);
		Sport_Recording.Service_Milage=AT24CXX_ReadLenByte(122,2);
		Sport_Recording.Second_Of_This_Trap = AT24CXX_ReadLenByte(33,2);
	}
	else//写单次里程数据
	{
		AT24CXX_WriteLenByte(25,Sport_Recording.Single_Trap_At_Once,2);
		delay_ms(5);
		AT24CXX_WriteLenByte(27,Sport_Recording.Timer_Of_This_Trap,2);
		delay_ms(5);
		AT24CXX_WriteLenByte(122,Sport_Recording.Service_Milage,2);
		delay_ms(5);
		AT24CXX_WriteLenByte(33, Timer_Second ,2);
		delay_ms(5);
	}
}
/*EEPROM 数据 写 —— 总里程备份*/
void Save_Total_Trap_Bak(void)
{
	if(Sport_Recording.Total_Trap_Record<10000000)
	{
		AT24CXX_WriteLenByte(29,Sport_Recording.Total_Trap_Record,4);
		delay_ms(5);
	}
}
/*EEPROM 数据 读写 —— 总里程*/
void Read_Save_Total_Trap_Record(u8 States)
{
	if(States)//读总里程数据   1677721
	{
		Sport_Recording.Total_Trap_Record = AT24CXX_ReadLenByte(21,4) & 0X00FFFFFF;
		if(Sport_Recording.Total_Trap_Record>10000000)//数据有误，读取备份数据
		{
			Sport_Recording.Total_Trap_Record = AT24CXX_ReadLenByte(29,4) & 0X00FFFFFF;
			if(Sport_Recording.Total_Trap_Record>10000000)//备份数据也乱了
			{
				Sport_Recording.Total_Trap_Record = 0;//清零ODO
				AT24CXX_WriteLenByte(21,0,4);
				delay_ms(5);
				Save_Total_Trap_Bak();
				Sport_Recording.Single_Trap_At_Once = 0;//清零trip time
				Sport_Recording.Timer_Of_This_Trap = 0;
				Sport_Recording.Service_Milage = 0;
				Read_Save_Single_Trap(0);
				
			}
		}
	}
	else//写总里程数据
		AT24CXX_WriteLenByte(21,Sport_Recording.Total_Trap_Record,4);
}

/*EEPROM 数据 读写 —— 最高速度*/
void Read_Save_MAX_Trap(u8 States)
{
	if(States)//读最大速度数据
	{
		Sport_Recording.MAX_Speed_In_This_Trap=AT24CXX_ReadLenByte(63,2);
		if(Sport_Recording.MAX_Speed_In_This_Trap>999)
		{
			Sport_Recording.MAX_Speed_In_This_Trap=0;
		}
	}
	else//写最大速度数据
	{
		AT24CXX_WriteLenByte(63,Sport_Recording.MAX_Speed_In_This_Trap,2);
		delay_ms(5);
	}
}

/*EEPROM 数据 读写 —— 开机密码*/
void Read_Save_SYS_PassWord(u8 States)
{
	u8 i,Trans_Buf[5];
	if(States)
	{
		AT24CXX_Read(1,Trans_Buf,5);
		for(i=0;i<4;i++)
			Sys_Code_Tab[i]=Trans_Buf[i];
		if(Trans_Buf[4]==0xaa)
			Work_Status.SYS_Start_PassWord_EnAble=1;
		else
			Work_Status.SYS_Start_PassWord_EnAble=0;
	}
	else
	{
		for(i=0;i<4;i++)
			Trans_Buf[i]=Sys_Code_Tab[i];
		if(Work_Status.SYS_Start_PassWord_EnAble)
			Trans_Buf[4]=0xaa;
		else
			Trans_Buf[4]=0;
		AT24CXX_Write(1,Trans_Buf,5);
		delay_ms(5);
	}
}
/*EEPROM 数据 读写 —— 设置参数*/
void Read_Save_SYS_Setting_Parameter(u8 States)
{
	u8 Trans_Buf[15];
	if(States)
	{
		AT24CXX_Read(6,Trans_Buf,15);
		Work_Status.SCA_Leve_Setting = Trans_Buf[0];
		Work_Status.BLK_Light_Level = Trans_Buf[1];
		Wheel_Dia_Mark = Trans_Buf[2];
		SYS_CUR_Limit_Value = Trans_Buf[3];
		Speed_Limte_Code = Trans_Buf[4];
		Work_Status.Power_Unit_PI_Flag = Trans_Buf[5];
		Scenes_Mode_Flag = Trans_Buf[6];
		SYS_Auto_OFF = Trans_Buf[7];
		SYS_Pus_FR = Trans_Buf[8];
		SYS_Pus_SCN = Trans_Buf[9];
		PAS_CP_CGN = Trans_Buf[10];
		SYS_P_V_Seclect = Trans_Buf[11];
		DL_Delay_Time = Trans_Buf[12];
		AUTO_LIGHT_Sensitivity = Trans_Buf[13];
		Work_Status.Speed_Unit_GY_Flag = Trans_Buf[14];
		
//		Config_Temp[7] = Work_Status.Power_Unit_PI_Flag;
//		Config_Temp[6] = Wheel_Dia_Mark;
//		Config_Temp[4] = SYS_P_V_Seclect;
//		Config_Temp[3] = Scenes_Mode_Flag;
//		Config_Temp[2] = SYS_Auto_OFF;
//		Config_Temp[1] = Work_Status.BLK_Light_Level;
//		Config_Temp[0] = Work_Status.Speed_Unit_GY_Flag;
	}
	else
	{
		Trans_Buf[0] = Work_Status.SCA_Leve_Setting;
		Trans_Buf[1] = Work_Status.BLK_Light_Level;
		Trans_Buf[2] = Wheel_Dia_Mark;
		Trans_Buf[3] = SYS_CUR_Limit_Value;
		Trans_Buf[4] = Speed_Limte_Code;
		Trans_Buf[5] = Work_Status.Power_Unit_PI_Flag;//SYS_Pus_CPn;
		Trans_Buf[6] = Scenes_Mode_Flag;
		Trans_Buf[7] = SYS_Auto_OFF;
		Trans_Buf[8] = SYS_Pus_FR;
		Trans_Buf[9] = SYS_Pus_SCN;
		Trans_Buf[10] = PAS_CP_CGN;
		Trans_Buf[11] = SYS_P_V_Seclect;
		Trans_Buf[12] = DL_Delay_Time;
		Trans_Buf[13] = AUTO_LIGHT_Sensitivity;
		Trans_Buf[14] = Work_Status.Speed_Unit_GY_Flag;
		AT24CXX_Write(6,Trans_Buf,15);
		delay_ms(5);
	}
}
/*EEPROM 数据 读写 —— 电压基准值*/
void Read_Save_VStandard(u8 States)
{
	if(States)
	{
		V_Standard_Value = (u16)AT24CXX_ReadLenByte(65,2);
		delay_ms(5);
	}
	else
	{
		AT24CXX_WriteLenByte(65 , (u32)V_Standard_Value , 2);
		delay_ms(5);
	}
}
/*EEPROM 数据 读写 —— 光感基准值*/
void Read_Save_AL_Standard(u8 States)
{
	if(States)
	{
		AL_Standard_Value = (u16)AT24CXX_ReadLenByte(67,2);
		delay_ms(5);
	}
	else
	{
		AT24CXX_WriteLenByte(67 , (u32)AL_Standard_Value , 2);
		delay_ms(5);
	}
}
/*参数、数据整体读写*/
void SaveAndRead_Data_By_aBigTab(u8 States)
{
	if(States==0x87)
	{
		Sys_New_Born_Flag=AT24CXX_ReadOneByte(0);
		if(Sys_New_Born_Flag==0xaa)
		{
			Read_Save_Total_Trap_Record(1);
			Read_Save_Single_Trap(1);
			Read_Save_MAX_Trap(1);
			Read_Save_SYS_PassWord(1);
			Read_Save_SYS_Setting_Parameter(1);
			//Read_Save_SCA_Value(1);
			Read_Save_Err_CODE(1);
			Read_Save_VStandard(1);
			Read_Save_AL_Standard(1);
			LOGO_Dis_Flag = 1;
		}
		else//系统第一次上电初始化
		{
			Sys_New_Born_Flag=0xaa;
			AT24CXX_WriteOneByte(0,0xaa);
			Read_Save_Total_Trap_Record(1);
			Read_Save_SYS_PassWord(1);
			Initialise_SYS_Parameter_Value();
			BF_SYS_Parameter_INT();
			States=0x25;
			LOGO_Dis_Flag = 0;
		}
	}
	if(States==0x25)
	{
//		INTX_DISABLE();//关闭所有中断
		Read_Save_SYS_Setting_Parameter(0);
		Read_Save_Total_Trap_Record(0);
		Read_Save_Single_Trap(0);
		Read_Save_MAX_Trap(0);
		Read_Save_SYS_PassWord(0);
		//Read_Save_SCA_Value(0);
		Read_Save_Err_CODE(0);
		Read_Save_VStandard(0);
//		INTX_ENABLE();//开启所有中断
	}
}

/*初始化主页面显示*/
void Int_Index_DisPlay(void)
{
	if(Work_Status.Speed_Unit_GY_Flag)
	{
			if(Speed_Limte_Code>38)
				SYS_Unit_S369_Flag = 1;
			else
				SYS_Unit_S369_Flag = 0;
	}
	else
	{
			if(Speed_Limte_Code>50)
				SYS_Unit_S369_Flag = 2;
			else if(Speed_Limte_Code>20)
				SYS_Unit_S369_Flag = 1;
			else
				SYS_Unit_S369_Flag = 0;
	}
	#ifdef POWER_YOUR_LIFE_LOGO_Words
	LCD_Fill_Back_Ground(Work_Status.Speed_Unit_GY_Flag , Work_Status.Power_Unit_PI_Flag , 0);
	#else
	LCD_Fill_Back_Ground(Work_Status.Speed_Unit_GY_Flag , Work_Status.Power_Unit_PI_Flag , 1);
	#endif
	LCD_DisPlay.PCC_ReLoad_Flag=1;
	LCD_DisPlay.Bat_ReLoad_Flag=1;
	LCD_DisPlay.Spd_ReLoad_Flag=1;
	CCS_Speed_Stro = CCS_Speed+1;
	Dis_The_Big_Point(1);
	LCD_DisPlay.PAS_ReLoad_Flag=1;
	Work_Status.Interface_ReLoad_Mask=1;
	LCD_DisPlay.TIME_ReLoad_Flag = 1;
	LCD_DisPlay.USB_ReLoad_Flag = 1;
	LCD_DisPlay.Light_ReLoad_Flag = 1;
}

/*速度显示平滑处理*/
u16 Speed_Smooth_BM(u16 Ispeed)
{
	if(Speed_Buf>Ispeed)
		Speed_Buf-=(((Speed_Buf - Ispeed)/3)+1);
	else
	{
		if(Speed_Buf<Ispeed)
			Speed_Buf+=(((Ispeed - Speed_Buf)/3)+1);
	}
	return Speed_Buf;
}
/*错误代码管理*/
void Set_ERR_CODE_M(u8 CODE_ERR)
{
	u8 i,j;
	for(i=0;i<S_Num_Err;i++)
	{
		if(CODE_ERR == Error_Code_Tab[i])//避免重复存储
		{
			for(j=0;j<i;j++)
			{
				Error_Code_Tab[i-j] = Error_Code_Tab[i-j-1];
			}
			Error_Code_Tab[0]=CODE_ERR;
			Read_Save_Err_CODE(0);
			return;
		}
	}
	for(i=0;i<9;i++)
		Error_Code_Tab[9-i]=Error_Code_Tab[8-i];
	Error_Code_Tab[0]=CODE_ERR;
	if(S_Num_Err<10)//错误代码计数
		S_Num_Err++;
	Error_Code_Tab[10]=S_Num_Err;
	Read_Save_Err_CODE(0);
}
/*显示各个分解项目*/
void LCD_DisPlay_ReLoad(void)
{
	if(LCD_DisPlay.WALK_ReLoad_Flag)
	{
		LCD_DisPlay.WALK_ReLoad_Flag=0;
		if(Work_Status.P_Walk_Flag)
			Dis_Play_Walk(1);
		else
		{
			Dis_Play_Walk(0);
			LCD_DisPlay.PAS_ReLoad_Flag=1;
		}
	}
	if(PAS_Value_NG)
	{
		PAS_Value_NG = 0;
		Dis_Play_ASS(PAS_Value,0);
		PAS_NG_Delay = 10;
	}
	if(PAS_Value_NNG)
	{
		PAS_Value_NNG = 0;
		LCD_DisPlay.PAS_ReLoad_Flag = 1;
	}
	if(Avg_JS_Flag)
	{
		Avg_JS_Flag = 0;
		if(Sport_Recording.Timer_Of_This_Trap>0 || Timer_Second>0)
			Sport_Recording.AVG_Speed_In_This_Trap=(Sport_Recording.Single_Trap_At_Once*3600)/((Sport_Recording.Timer_Of_This_Trap*60) + Timer_Second + Sport_Recording.Second_Of_This_Trap);
		else
			Sport_Recording.AVG_Speed_In_This_Trap=0;
		if(AVG_Buf!=Sport_Recording.AVG_Speed_In_This_Trap)
		{
			AVG_Buf = Sport_Recording.AVG_Speed_In_This_Trap;
			if(Work_Status.Working_Mode == 3)
				Work_Status.Interface_ReLoad_Mask = 1;
		}
	}
	if(Setting.Exit_Flag)
	{
		Int_Index_DisPlay();/*页面显示初始化*/
		Setting.Exit_Flag=0;
		Work_Status.Interface_ReLoad_Mask=1;
		if(Wheel_Dia_Mark==27)
			Wheel_Perimeter = 2193;
		else
			Wheel_Perimeter = (u32)(Wheel_Dia_Mark*79756/1000);
		KM_Speed_Base_Value = Wheel_Perimeter*36;
		MH_Speed_Base_Value = Wheel_Perimeter*360/16;
		if(Work_Status.Speed_Unit_GY_Flag)
			Speed_Base_Value=MH_Speed_Base_Value;
		else
			Speed_Base_Value=KM_Speed_Base_Value;
		if(Work_Status.SYS_Err_Occurred_Flag)
			LCD_DisPlay.ERR_ReLoad_Flag=1;
		if(Work_Status.Light_Switch_Flag)
			LCD_DisPlay.Light_ReLoad_Flag = 1;
		if(Work_Status.USB_State_Flag)
			LCD_DisPlay.USB_ReLoad_Flag = 1;
	}
	if(LCD_DisPlay.E_S_SW_Flag)
	{
		LCD_DisPlay.E_S_SW_Flag = 0;
		LCD_DisPlay.Spd_ReLoad_Flag = 1;
		CCS_Speed_Stro = 1;
		CCS_Speed = 0;
		LCD_DisPlay.PCC_ReLoad_Flag = 1;
		DisPlay_ECO_SPORT_SWITCH(Work_Status.Speed_Unit_GY_Flag , Work_Status.Power_Unit_PI_Flag , Work_Status.SYS_Err_Occurred_Flag);
	}
	if(Work_Status.Interface_ReLoad_Mask)
	{
		switch(Work_Status.Working_Mode)
		{
			case 0:
				Show_Word_Agency(1,65,0,"TRIP " , White , Black);
				if(Work_Status.Speed_Unit_GY_Flag)
				{
					Dis_MODE_NumS((u32)(Sport_Recording.Single_Trap_At_Once*62/100) , 1 , 38);
					Show_Word_DUnits(4);//mil
				}
				else
				{
					Dis_MODE_NumS(Sport_Recording.Single_Trap_At_Once , 1 , 26);
					Show_Word_DUnits(0);//km
				}
			break;
			case 1:
				Show_Word_Agency(1,65,0,"ODO  " , White , Black);
				if(Work_Status.Speed_Unit_GY_Flag)
				{
					Dis_MODE_NumS((u32)(Sport_Recording.Total_Trap_Record*62/1000) , 0 , 38);
					Show_Word_DUnits(4);//mil
				}
				else
				{
					Dis_MODE_NumS(Sport_Recording.Total_Trap_Record/10 , 0 , 26);
					Show_Word_DUnits(0);//km
				}
			break;
			case 2:
				Show_Word_Agency(1,65,0,"MAX " , White , Black);
				if(Work_Status.Speed_Unit_GY_Flag)
				{
					Dis_MODE_NumS((Sport_Recording.MAX_Speed_In_This_Trap*62/100) , 2 , 38);
					Show_Word_DUnits(5);//mph
				}
				else
				{
					Dis_MODE_NumS(Sport_Recording.MAX_Speed_In_This_Trap , 2 , 45);
					Show_Word_DUnits(1);//km/h
				}
			break;
			case 3:
				Show_Word_Agency(1,65,0,"AVG " , White , Black);
				if(Work_Status.Speed_Unit_GY_Flag)
				{
					Dis_MODE_NumS((u32)(Sport_Recording.AVG_Speed_In_This_Trap*62/100) , 2 , 38);
					Show_Word_DUnits(5);//mph
				}
				else
				{
					Dis_MODE_NumS(Sport_Recording.AVG_Speed_In_This_Trap , 2 , 45);
					Show_Word_DUnits(1);//km/h
				}
			break;
			case 4://剩余里程
				Show_Word_Agency(1,65,0,"RANGE" , White , Black);
				if(Work_Status.Speed_Unit_GY_Flag)
				{
//					if(PAS_Value==0)
					if(Sport_Recording.Trap_Range==0xffff)
						Dis_MODE_NumS(0xffffffff , 0 , 38);
					else
						Dis_MODE_NumS((u32)(Sport_Recording.Trap_Range*62/100) , 0 , 38);
					Show_Word_DUnits(4);//mil
				}
				else
				{
//					if(PAS_Value==0)
					if(Sport_Recording.Trap_Range==0xffff)
						Dis_MODE_NumS(0xffffffff , 0 , 26);
					else
						Dis_MODE_NumS(Sport_Recording.Trap_Range , 0 , 26);
					Show_Word_DUnits(0);//km
				}
			break;
			case 5://calorie卡路里
				Show_Word_Agency(1,65,0,"CALORIES" , White , Black);
				Dis_MODE_NumS(Sport_Recording.S_Calorie , 0 , 35);
				Show_Word_DUnits(2);//kcal
			break;
			case 6:
				Show_Word_Agency(1,65,0,"TIME       " , White , Black);
				Dis_MODE_NumS(Time_For_BF_ShutWithClear , 0 , 30);
				Show_Word_DUnits(3);//min
			break;
		}
		Work_Status.Interface_ReLoad_Mask=0;
	}
	if(LCD_DisPlay.Bat_ReLoad_Flag)//显示电量
	{
		LCD_DisPlay.Bat_ReLoad_Flag=0;
		if(Setting.SOC_Rev_Flag)
			Dis_Power_Left_Continue(Battery_SOC);
		else
		{
//			Dis_Power_Left_Continue(SYS_Voltage_Start-385);
//			Battery_SOC = SYS_Voltage_Start-385;
			Dis_Power_Left_Continue(0);
			Battery_SOC = 0;
		}
		Dis_Valtage_Nums(SYS_P_V_Seclect,Battery_SOC,SYS_Voltage_Start);
	}
	if(LCD_DisPlay.Spd_ReLoad_Flag && (Work_Status.SYS_Err_Occurred_Flag==0))//显示速度
	{
		LCD_DisPlay.Spd_ReLoad_Flag=0;
		CCS_Speed=Speed_Smooth_BM(D_Speed);
		if(CCS_Speed_Stro != CCS_Speed)
		{
			CCS_Speed_Stro = CCS_Speed;
			Dis_Play_Speeds(CCS_Speed);
		}
	}
	if(LCD_DisPlay.PAS_ReLoad_Flag)//显示档位值 及 运动模式
	{
		LCD_DisPlay.PAS_ReLoad_Flag=0;
		Dis_Play_ASS(PAS_Value,1);
	}
	if(LCD_DisPlay.TIME_ReLoad_Flag)//显示时间
	{
		LCD_DisPlay.TIME_ReLoad_Flag = 0;
		DisPlay_Real_Time(Timer_Hour , Timer_Minute);
//		DisPlay_HMS_Time(Timer_Hour , Timer_Minute , Timer_Second);
	}
	if(LCD_DisPlay.PCC_ReLoad_Flag && (Work_Status.SYS_Err_Occurred_Flag==0))//显示功率
	{
		LCD_DisPlay.PCC_ReLoad_Flag=0;
		Dis_Play_POWER_CC(Motor_Power_CC);
	}
	if(LCD_DisPlay.Light_ReLoad_Flag)//显示大灯符号
	{
		LCD_DisPlay.Light_ReLoad_Flag = 0;
		Show_DD_Syb(Work_Status.Light_Switch_Flag);
	}
	if(LCD_DisPlay.ERR_ReLoad_Flag)//故障代码显示
	{
		LCD_DisPlay.ERR_ReLoad_Flag=0;
		if((Work_Status.Current_Err_CODE<4) && (Work_Status.Current_Err_CODE!=0x22))
		{
			if(Work_Status.SYS_Err_Occurred_Flag)
			{
				Work_Status.SYS_Err_Occurred_Flag=0;
//				LCD_DisPlay.Spd_ReLoad_Flag = 1;
				CCS_Speed_Stro = 10;
				DisPlay_Err_Message(0);
				Int_Index_DisPlay();/*页面显示初始化*/
			}
		}
		else
		{
			if(((Work_Status.Current_Err_CODE>3)&&(Work_Status.Current_Err_CODE<10))||((Work_Status.Current_Err_CODE>15)&&(Work_Status.Current_Err_CODE<22))||\
				((Work_Status.Current_Err_CODE>34)&&(Work_Status.Current_Err_CODE<39))||(Work_Status.Current_Err_CODE==33)||(Work_Status.Current_Err_CODE==48)&&(Work_Status.Current_Err_CODE!=6))
			{
				if(Work_Status.Engineering_Mode_MASK!=1)
				{
					Work_Status.SYS_Err_Occurred_Flag=1;
					Set_ERR_CODE_M(Work_Status.Current_Err_CODE);
					DisPlay_Err_Message(Work_Status.Current_Err_CODE);
				}
			}
		}
	}
	if(LCD_DisPlay.USB_ReLoad_Flag)
	{
		LCD_DisPlay.USB_ReLoad_Flag = 0;
		DiaPlay_USB(Work_Status.USB_State_Flag);
	}
}
/*----System ShutDown-----**关机操作**--------*/
void Sys_ShutUP_I(void)
{
	Set_BK_Light_Leve(0);//关闭背光
	LCD_Clear(Black);
	Work_Status.SYS_Power_States_Flag=0;
	HAL_TIM_Base_Stop(&htim2);//关闭定时器
	HAL_TIM_Base_Stop(&htim3);
	Read_Save_Total_Trap_Record(0);//保存数据
	Read_Save_Single_Trap(0);
	Read_Save_MAX_Trap(0);
	delay_ms(5);
	Save_Total_Trap_Bak();
	delay_ms(5);
	PAS_Value=0;//发送0档
	Send_PAS_Value();
	delay_ms(50);
	System_Power_OFF;
	while(!Key_MOD);//等待按键松开
	while(1)
	{
		if(!Key_MOD)//等待按键松开
		{
			delay_ms(100);
		}
	}
}
/*----System Sleep -----**-USB充电模式下的系统休眠-**-------*/
void Sys_Sleep_USB(void)
{
	u8 s = 0 , PAS_USB_Buffer;
	PAS_USB_Buffer = PAS_Value;
	LCD_Clear(Black);
	DiaPlay_USB(1);
	Set_BK_Light_Leve(1);
	Show_Word_Agency(10,240,0,(u8 *)"USB Charging..." , White , Black);
	PAS_Value = 0;
	Send_PAS_Value();
	delay_ms(200);
	Send_PAS_Value();
	while(1)
	{
		if(!Work_Status.USB_State_Flag)
			Sys_ShutUP_I();
		if(!Key_MOD)
		{
			if(++s > 100)
			{
				s = 0;
				PAS_Value = PAS_USB_Buffer;
				Work_Status.ShutDown_Flag = 0;
				Work_Status.USB_Sleep_Flag = 0;
				Sys_Auto_ShutUp_Delay = 0;
				LCD_DisPlay.USB_ReLoad_Flag = 1;
				Work_Status.SYS_Err_Occurred_Flag=0;
				Set_BK_Light_Leve(Work_Status.BLK_Light_Level);
				Int_Index_DisPlay();
				break;
			}
		}
		delay_ms(10);
	}
}
/*USART-SEND ----> 发送助力档位值*/
void Send_PAS_Value(void)
{
	Send_Data[0]=0x16;
	Send_Data[1]=0x0B;
	if(PAS_Value>0)
		Send_Data[2]=PAS_Tab_Set[PAS_Value-1];
	else
		Send_Data[2]=0;
	if(Work_Status.P_Walk_Flag)//Walk 模式
		Send_Data[2]=6;
	Send_Data[3]=(u8)(0x16+0x0B+Send_Data[2]);
	USART_Send_Data(Send_Data,4);
}
/*USART-SEND ----> 发送PC呼叫代码*/
void Call_PC_Code(void)
{
	Send_Data[0]=0x11;
	Send_Data[1]=0x90;
	USART_Send_Data(Send_Data,2);
}
/*USART-SEND ----> 发送客户号*/
void Send_PC_Gust(void)
{
	uint8_t i , t , GS_Len , G_Check0=0;
	GS_Len = BF_Gust_Sirl[0]+1;
	G_Tab_Temp[0]=0x11;
	G_Tab_Temp[1]=0x0B;
	G_Check0+=0x1C;
	for(i=0;i<GS_Len;i++)
	{
		t=BF_Gust_Sirl[i];
		G_Tab_Temp[i+2]=t;
		G_Check0+=t;
	}
	G_Tab_Temp[GS_Len+2]=G_Check0;
	USART_Send_Data(G_Tab_Temp,GS_Len+3);
}
/*USART-SEND ----> 发送生产序列号*/
void Send_PC_Product_SN(void)
{
	uint8_t i , t , GS_Len , G_Check0=0;
	GS_Len = BF_Product_SN[0]+1;
	G_Tab_Temp[0]=0x14;
//	G_Tab_Temp[1]=GS_Len;
	G_Check0+=0x14;
	for(i=0;i<GS_Len;i++)
	{
		t=BF_Product_SN[i];
		G_Tab_Temp[i+1]=t;
		G_Check0+=t;
	}
	G_Tab_Temp[GS_Len+1]=G_Check0;
	USART_Send_Data(G_Tab_Temp,GS_Len+2);
}
/*USART-SEND ----> 发送车架号*/
void Send_PC_Frame_SN(void)
{
	uint8_t i , t , GS_Len , G_Check0=0;
	GS_Len = BF_Frame_SN[0]+1;
	G_Tab_Temp[0]=0x11;
//	G_Tab_Temp[1]=GS_Len;
	G_Check0+=0x11;
	for(i=0;i<GS_Len;i++)
	{
		t=BF_Frame_SN[i];
		G_Tab_Temp[i+1]=t;
		G_Check0+=t;
	}
	G_Tab_Temp[GS_Len+1]=G_Check0;
	USART_Send_Data(G_Tab_Temp,GS_Len+2);
}
/*USART-SEND ----> 发送售后里程*/
void Send_PC_Sevice_Milage(void)
{
	uint8_t G_Check0=0;
	G_Tab_Temp[0]=0x16;
	G_Tab_Temp[1]=0x02;
	G_Check0+=0x18;
	G_Tab_Temp[2]=Sport_Recording.Service_Milage/256;
	G_Check0+=G_Tab_Temp[2];
	G_Tab_Temp[3]=Sport_Recording.Service_Milage%256;
	G_Check0+=G_Tab_Temp[3];
	G_Tab_Temp[4]=G_Check0;
	USART_Send_Data(G_Tab_Temp,5);
}

/*USART-SEND ----> 发送参数Beest*/
void Send_Beest_Para(void)
{
	uint8_t i , t;
	G_Tab_Temp[0]=0x11;
	G_Tab_Temp[1]=0x0D;
	G_Tab_Temp[2]=0x03;
	G_Tab_Temp[3]=0x00;
	G_Tab_Temp[4]=Wheel_Dia_Mark;
	G_Tab_Temp[5]=Speed_Limte_Code + 10;
	t=0;
	for(i=0;i<6;i++)
		t+=G_Tab_Temp[i];
	G_Tab_Temp[6]=t;
	USART_Send_Data(G_Tab_Temp,7);
}
/*USART-SEND ----> 发送工作模式*/
void Send_Scenes_Mode(void)
{
	Send_Data[0]=0x16;
	Send_Data[1]=0x0C;
	if(Scenes_Mode_Flag)
	{
		Send_Data[2]=0X04;
		Send_Data[3]=0X26;
	}
	else
	{
		Send_Data[2]=0X02;
		Send_Data[3]=0X24;
	}
	USART_Send_Data(Send_Data,4);
}

/*USART-SEND ----> 串口命令 设置限速*/
void Send_Speed_Limit_Value(void)
{
	u16 TM;
	u8  TM_L,TM_H;
	Send_Data[0]=0x16;
	Send_Data[1]=0x1f;
	TM=(u16)((u32)(16667*(Speed_Limte_Code+10))/Wheel_Perimeter);
	TM_H=TM/256;
	TM_L=TM%256;
	Send_Data[2]=TM_H;
	Send_Data[3]=TM_L;
	Send_Data[4]=(u8)(0x35+TM_H+TM_L);
	USART_Send_Data(Send_Data,5);
}

/*USART-SEND ----> 串口命令 读取电池信息*/
void Read_battey_Index(u8 S)
{
	Send_Data[0] = 0x11;
	if(S)
	{
		Send_Data[1] = 0x60;
		Send_Data[2] = 0x71;
	}
	else
	{
		Send_Data[1] = 0x61;
		Send_Data[2] = 0x72;
	}
	USART_Send_Data(Send_Data,3);
}

/*USART-SEND =====>> 串口发送数据轮询*/
void DisPlay_Send_Running_Data(void)
{
	if((MY_USART.Need_Rev_Data_Mask==0) || MY_USART.RevBack_Time_Out_Flag)
	{
		if(Send_Mode<8)
		{
				Send_Mode++;//发送常规数据
				if(Send_Mode==6)
				{
						if(LCD_DisPlay.RANGE_DIS_EN == 0)
								Send_Mode++;
				}
				if(Send_Mode==7)
				{
						if(LCD_DisPlay.CALORIES_DIS_EN == 0)
								Send_Mode++;
				}
				if(Send_Mode == 8)
				{
						if(MY_USART.Send_Scenes_Flag == 0)
							Send_Mode++;
				}
		}
		else
		{
			if(Send_Mode_Deep==0)
			{
				Send_Mode_Deep=1;
				Send_Mode=0;
				if(Send_Mode_S<5)//发送特殊数据
					Send_Mode_S++;
				else
					Send_Mode_S=1;
			}
			else
			{
				Send_Mode_Deep=0;
				Send_Mode=1;
			}
		}
		if(Send_Mode>0)
		{
			Send_Data[0]=0x11;
			if(Send_Mode==1)
			{
				Send_Data[1]=0x08;//读状态
				Control_ANS_Flag=1;
			}
			else if(Send_Mode==2)
			{
				Send_Data[1]=0x0A;//读瞬时电流
				Control_ANS_Flag=2;
			}
			else if(Send_Mode==3)
			{
				Send_Data[1]=0x20;//读速度
				Control_ANS_Flag=3;
			}
			else if(Send_Mode==5)
			{
				Send_Data[1]=0x31;//读工作状态
				Control_ANS_Flag=10;
			}
			else if(Send_Mode==4)
			{
				Send_Data[1]=0x11;//读SOC
				Control_ANS_Flag=4;
			}
			else if(Send_Mode==6)
			{
				Send_Data[0] = 0x11;
				Send_Data[1] = 0x22;//读剩余里程
				Send_Data[2] = 0x33;//读剩余里程
				Call_Reply_Mask = 1;
				Control_ANS_Flag=5;
				MY_USART.Need_Rev_Data_Mask = 1;
				USART_Send_Data(Send_Data,3);
			}
			else if(Send_Mode==7)
			{
				Send_Data[0] = 0x11;
				Send_Data[1] = 0x24;//读卡路里
				Send_Data[2] = 0x35;//读卡路里
				Call_Reply_Mask = 1;
				Control_ANS_Flag=6;
				MY_USART.Need_Rev_Data_Mask = 1;
				USART_Send_Data(Send_Data,3);
			}
			else if(Send_Mode==8)
			{
				Call_Reply_Mask = 1;
				Control_ANS_Flag=20;
				MY_USART.Need_Rev_Data_Mask = 1;
				Send_Scenes_Mode();
			}
			if(Send_Mode<6)
			{
				MY_USART.Need_Rev_Data_Mask = 1;
				USART_Send_Data(Send_Data,2);
			}
		}
		else if(Send_Mode==0)
		{
			Control_ANS_Flag=9;
			if(Send_Mode_S==1)//发送PAS值
			{
				Send_PAS_Value();
				MY_USART.Need_Rev_Data_Mask = 0;
			}
			else if(Send_Mode_S==2)//发送  限速 转速
			{
				Send_Speed_Limit_Value();
				MY_USART.Need_Rev_Data_Mask = 0;
			}
			else if(Send_Mode_S==3)//发送大灯状态
			{
				Send_Data[0]=0x16;
				Send_Data[1]=0x1A;
				if(Work_Status.Light_Switch_Flag)
				{
					Send_Data[2]=0xF1;
//					Show_Word_Agency(10,400,0,"L",Red,Black);
				}
				else
				{
					Send_Data[2]=0xF0;
//					Show_Word_Agency(10,400,0,"G",Red,Black);
				}
				USART_Send_Data(Send_Data,3);
				MY_USART.Need_Rev_Data_Mask = 0;
			}
			else if(Send_Mode_S==4)
			{
				Send_Data[0] = 0x11;
				Send_Data[1] = 0x21;//读踏频
				Send_Data[2] = 0x32;//读踏频
				Control_ANS_Flag=7;
				MY_USART.Need_Rev_Data_Mask = 0;
				USART_Send_Data(Send_Data,3);
			}
			else if(Send_Mode_S==5)
			{
				Send_Data[0] = 0x11;
				Send_Data[1] = 0x25;//读力矩传感器数据
				Send_Data[2] = 0x36;//读力矩传感器数据
				Control_ANS_Flag=8;
				MY_USART.Need_Rev_Data_Mask = 0;
				USART_Send_Data(Send_Data,3);
			}
		}
		MY_USART.RevBack_Time_Out = TIME_OF_REV_OUT;
		MY_USART.RevBack_Time_Out_Flag = 0;
		MY_USART.Just_Send_Flag = 1;
	}
}

/*USART-REV ---->  串口接收数据处理*/
void J_DisPlay_Receive_CallBack(void)
{
	uint8_t  PC_Commond , i=0 , j=0 , vcl[5];
	uint8_t  Sum_Check_M , Sum_Check0;
	uint32_t V_Temp6 , C_Temp10[20];
	uint16_t M_Speed;
	Work_Status.NoCommunication_OverFlow=0;
	MY_USART.REV_Back_OUT_Time = 0;
	if(Work_Status.Current_Err_CODE == 0x30)
	{
		Work_Status.Current_Err_CODE=0;
		Work_Status.SYS_Err_Occurred_Flag=0;
		Int_Index_DisPlay();
	}
	if((Rx_Buf[0]==0x3a) && (Rx_Buf[1]==0x1c) && (Rx_Buf[RX_NUM-1]==0x0a) && (Rx_Buf[RX_NUM-2]==0x0d))//判断是否PC端配置软件发来的数据
	{
		RX_NUM-=4;
		Sum_Check0=Rx_Buf[RX_NUM];
		RX_NUM--;
		Sum_Check_M=0;
		while(RX_NUM>0)//数据校验
		{
			Sum_Check_M+=Rx_Buf[RX_NUM];
			RX_NUM--;
		}
		if(Sum_Check_M==Sum_Check0)//校验和检验
		{
			Work_Status.Call_IN_From_PC_Flag=1;
			Work_Status.Engineering_Mode_MASK = 1;
			PC_Commond=Rx_Buf[2];
		}
	}
	else
	{
		Work_Status.Call_IN_From_PC_Flag=0;
//		Calibration_V_Standard=0;
	}
    RX_NUM=0;
  if((Work_Status.Call_IN_From_PC_Flag==0) && (MY_USART.RevBack_Time_Out_Flag==0) && (MY_USART.Need_Rev_Data_Mask==1))//正常通信，通信未超时
	{
		if((Control_ANS_Flag==1) && (Rx_Len==1))
		{
			MY_USART.Need_Rev_Data_Mask = 0;
			MY_USART.RevBack_Time_Out = 0;
			MY_USART.RevBack_Time_Out_Flag = 0;
			Work_Status.Current_Err_CODE=Rx_Buf[0];//判断错误标识
			if(Work_Status.Current_Err_CODE_Bk != Work_Status.Current_Err_CODE)
			{
				Work_Status.Current_Err_CODE_Bk=Work_Status.Current_Err_CODE;
				LCD_DisPlay.ERR_ReLoad_Flag=1;
			}
		}
		else if((Control_ANS_Flag==2) && (Rx_Len==2))
		{
			MY_USART.Need_Rev_Data_Mask = 0;
			MY_USART.RevBack_Time_Out = 0;
			MY_USART.RevBack_Time_Out_Flag = 0;
			C_Current=Rx_Buf[0];
			if(C_Current==Rx_Buf[1])
			{
				if(Work_Status.Power_Unit_PI_Flag)//显示电流
				{
					Motor_Power_CC=(u16)(C_Current*2);
				}
				else
				{
					Motor_Power_CC=(u16)((SYS_Voltage*C_Current)/20);
//					Motor_Power_CC*=31;
//					Motor_Power_CC/=375;
					Motor_Power_CC/=12;
				}
				if(Motor_Power_CC>123)
					Motor_Power_CC=123;
				if(Motor_Power_CC!=Motor_Power_CC_Br)//显示功率
				{
					Motor_Power_CC_Br=Motor_Power_CC;
					LCD_DisPlay.PCC_ReLoad_Flag=1;
				}
			}
		}
		else if((Control_ANS_Flag==3)  && (Rx_Len==3))
		{
			Sum_Check0 = (u8)(Rx_Buf[0]+Rx_Buf[1]+0x20);
			if(Sum_Check0 == Rx_Buf[2])
			{
				MY_USART.Need_Rev_Data_Mask = 0;
				MY_USART.RevBack_Time_Out = 0;
				MY_USART.RevBack_Time_Out_Flag = 0;
				Wheel_T=(unsigned int)((Rx_Buf[0]<<8)+Rx_Buf[1]);//显示速度
				if(Wheel_T>10)
				{
					Wheel_T=60000/Wheel_T;
					Mileage_Variable.Base_Time_Count_Delay=Wheel_T;//改变里程计数时基
				}
				else
					Wheel_T=60000;
				if(Wheel_T>3499)//车速为0
				{
					Work_Status.Bike_Moving_Flag=0;
					Work_Status.Current_Speed=0;
				//	Get_Speed_AVGSD(0);
				}
				else
				{
					Work_Status.Bike_Moving_Flag=1;
					
					Work_Status.Current_Speed=(Speed_Base_Value/Wheel_T);
					
					if(Work_Status.Speed_Unit_GY_Flag)//当前为英制数据
					{
							M_Speed = (Work_Status.Current_Speed * 100)/62;
							if((M_Speed*62/100)<Work_Status.Current_Speed)
								M_Speed+=1;
					}
					else
							M_Speed = Work_Status.Current_Speed;
					
					if(Sport_Recording.MAX_Speed_In_This_Trap<M_Speed)//更新最高速度
					{
						Sport_Recording.MAX_Speed_In_This_Trap=M_Speed;
						if(Work_Status.Working_Mode == 2)
							Work_Status.Interface_ReLoad_Mask=1;
						Work_Status.MAX_SPD_Reset=1;
					}
				}
				if(EMC_TEST.EN_Flag)
				{
					if(Work_Status.Current_Speed > EMC_TEST.EMC_Sped_Buf)//EMC模式下，速度变化超过3km/h，则使用EMC记忆速度
					{
						if((Work_Status.Current_Speed - EMC_TEST.EMC_Sped_Buf)>30)
							D_Speed=EMC_TEST.EMC_Sped_Buf;
						else
							D_Speed=Work_Status.Current_Speed;//实时速度
					}
					else
					{
						if((EMC_TEST.EMC_Sped_Buf - Work_Status.Current_Speed)>30)
							D_Speed=EMC_TEST.EMC_Sped_Buf;
						else
							D_Speed=Work_Status.Current_Speed;//实时速度
					}
					Work_Status.Bike_Moving_Flag=1;
				}
				else
					D_Speed=Work_Status.Current_Speed;//实时速度
//				LCD_DisPlay.Spd_ReLoad_Flag = 1;
			}
		}
		else if((Control_ANS_Flag==4) && (Rx_Len==2)) //-----------------SOC
		{
			if(Rx_Buf[0]==Rx_Buf[1])
			{
				Battery_SOC=Rx_Buf[0];
				if(Battery_SOC>0)
				{
					if(Battery_SOC>99)
						Battery_SOC=100;
					Setting.SOC_Rev_Flag = 1;
					if(Battery_SOC_FR != Battery_SOC)
					{
							if(++Battery_SOC_Flit>1)
							{
								Battery_SOC_Flit = 0;
								Battery_SOC_FR = Battery_SOC;
								LCD_DisPlay.Bat_ReLoad_Flag = 1;
							}
					}
					MY_USART.Need_Rev_Data_Mask = 0;
					MY_USART.RevBack_Time_Out = 0;
					MY_USART.RevBack_Time_Out_Flag = 0;
				}
//				else
//					Show_Word_Agency(10,400,0,"S0",Red,Black);
			}
		}
		else if((Control_ANS_Flag==5) && (Rx_Len==3)) //-----------------Range
		{
			Sum_Check0 = (u8)(Rx_Buf[0]+Rx_Buf[1]);
			if(Sum_Check0 == Rx_Buf[2])
			{
				Rx_Buf[2] = 0;
				LCD_DisPlay.R_S_Flag = 0;
				MY_USART.Need_Rev_Data_Mask = 0;
				MY_USART.RevBack_Time_Out = 0;
				MY_USART.RevBack_Time_Out_Flag = 0;
				Sport_Recording.Trap_Range=(unsigned int)((Rx_Buf[0]<<8)+Rx_Buf[1]);//更新剩余里程
//				if(Sport_Recording.Trap_Range>9999)
//					Sport_Recording.Trap_Range = 0;
				if(Work_Status.Working_Mode == 4)
				{
					if(Sport_Recording.Trap_Range_Fr != Sport_Recording.Trap_Range)
					{
						Sport_Recording.Trap_Range_Fr = Sport_Recording.Trap_Range;
						Work_Status.Interface_ReLoad_Mask=1;
					}
				}
			}
		}
		else if((Control_ANS_Flag==6) && (Rx_Len==3)) //-----------------Calorie
		{
			Sum_Check0 = (u8)(Rx_Buf[0]+Rx_Buf[1]);
			if(Sum_Check0 == Rx_Buf[2])
			{
				Rx_Buf[2] = 0;
				LCD_DisPlay.C_S_Flag = 0;
				MY_USART.Need_Rev_Data_Mask = 0;
				MY_USART.RevBack_Time_Out = 0;
				MY_USART.RevBack_Time_Out_Flag = 0;
				Sport_Recording.S_Calorie=(unsigned int)((Rx_Buf[0]<<8)+Rx_Buf[1]);//更新卡路里值
				if(Sport_Recording.S_Calorie>59999)
					Sport_Recording.S_Calorie = 59999;
				if(Work_Status.Working_Mode == 5)
				{
					if(Sport_Recording.S_Calorie_Fr!=Sport_Recording.S_Calorie)
					{
						Sport_Recording.S_Calorie_Fr=Sport_Recording.S_Calorie;
						Work_Status.Interface_ReLoad_Mask=1;
					}
				}
			}
		}
		else if((Control_ANS_Flag==10) && (Rx_Len==2)) //-----------------工作状态
		{
			Moto_Working_Flag = 0;
			if(Rx_Buf[0] == Rx_Buf[1])
			{
				MY_USART.Need_Rev_Data_Mask = 0;
				MY_USART.RevBack_Time_Out = 0;
				MY_USART.RevBack_Time_Out_Flag = 0;
				if(Rx_Buf[0] == 0x31)
					Moto_Working_Flag = 1;
				else if(Rx_Buf[0] == 0x30)
				{
					Moto_Working_Flag = 0;
					if(EMC_TEST.EN_Flag)
					{
						if(++EMC_TEST.EMC_Exit_Delay2>0)
						{
							EMC_TEST.EMC_Exit_Delay2 = 0;
							EMC_TEST.EN_Flag = 0;
						}
					}
				}
			}
		}
		else if((Control_ANS_Flag==20) && (Rx_Len==4)) 
		{
				MY_USART.Need_Rev_Data_Mask = 0;
				MY_USART.RevBack_Time_Out = 0;
				MY_USART.RevBack_Time_Out_Flag = 0;
				MY_USART.Send_Scenes_Flag = 0;
		}
		Control_ANS_Flag = 0;
		Rx_Len = 0;
		Rx_Cnt = 0;
	}
	else//--------处理PC指令----------------
	{
		if(PC_Commond==0x5d)//电压校准指令
		{
			ADC_Value[147] = 0;
			Clr_TFTLCD_Area(0,200,319,280,White1);
			Show_Word_Agency(10,250,0,"Voltage Calibration..." ,Red , White1);
			delay_ms(5);
			while(ADC_Value[147] == 0)
			;
			V_Temp6 = 0;
			for(i=0;i<50;i++)
				V_Temp6 += ADC_Value[3*i];
			V_Standard_Value = V_Temp6/360;
			Read_Save_VStandard(0);
			vcl[0]=V_Standard_Value/100%10 + '0';
			vcl[1]=V_Standard_Value/10%10 + '0';
			vcl[2]=V_Standard_Value%10 + '0';
			vcl[3]=0;
			Show_Word_Agency(10,210,0,"Reference:" ,Red , White1);
			Show_Word_Agency(190,210,0,vcl , Green , White1);
			delay_ms(500);
			Int_Index_DisPlay();
		}
		else if(PC_Commond==0x6E)
		{
			//以下校准光感流程
			Clr_TFTLCD_Area(0,200,319,280,White1);
			Show_Word_Agency(10,250,0,"AL Calibration..." ,Red , White1);
			for(j=0;j<20;j++)
			{
					ADC_Value[149] = 0;
					delay_ms(10);
					while(ADC_Value[149] == 0)
					;
					V_Temp6 = 0;
					for(i=0;i<50;i++)
						V_Temp6 += ADC_Value[2 + (3*i)];
					C_Temp10[j] = V_Temp6/50;
			}
			V_Temp6 = 0;
			for(j=0;j<20;j++)
			{
					V_Temp6 += C_Temp10[j];
			}
			V_Temp6/=20;
			if(V_Temp6>3000)
			{
					AL_Standard_Value = V_Temp6;
					Read_Save_AL_Standard(0);
					vcl[0]=AL_Standard_Value/1000%10 + '0';
					vcl[1]=AL_Standard_Value/100%10 + '0';
					vcl[2]=AL_Standard_Value/10%10 + '0';
					vcl[3]=AL_Standard_Value%10 + '0';
					vcl[4]=0;
					Show_Word_Agency(10,210,0,"Reference:" ,Red , White1);
					Show_Word_Agency(190,210,0,vcl , Green , White1);
			}
			else
					Show_Word_Agency(10,210,0,"Cover The Sensor!" ,Red , White1);
			delay_ms(800);
			Int_Index_DisPlay();
		}
		else if(PC_Commond==0x71) // 校准时间,时/分/秒   同时校准光感
		{
			Timer_Hour = Rx_Buf[4];
			Timer_Minute = Rx_Buf[5];
			Timer_Second = Rx_Buf[6];
			RTC_Set_Time(Timer_Hour , Timer_Minute , Timer_Second);
			LCD_DisPlay.TIME_ReLoad_Flag = 1;
			
		}
		else if(PC_Commond==0x8E) // 清零 总里程数据  ODO Clear
		{
			Show_Word_Agency(260,110,0,"OK", Green , Black);
			Sport_Recording.Total_Trap_Record=0;
			Read_Save_Total_Trap_Record(0);
			delay_ms(5);
			Save_Total_Trap_Bak();
			delay_ms(5);
			Sport_Recording.Single_Trap_At_Once = 0;
			Sport_Recording.Timer_Of_This_Trap = 0;
			Read_Save_Single_Trap(0);
			delay_ms(5);
			Sport_Recording.MAX_Speed_In_This_Trap=0;
			Read_Save_MAX_Trap(0);
			Work_Status.Interface_ReLoad_Mask=1;
			for(i=0;i<11;i++)//清除错误代码
				Error_Code_Tab[i]=0;
			S_Num_Err = 0;
			Read_Save_Err_CODE(0);
			Show_Word_Agency(260,110,0,"    ", Green , Black);
		}
		Work_Status.Call_IN_From_PC_Flag=0;
	}
}

/*SETTINGS ---->  设置界面处理*/
void Setting_Menu_List(void)
{
	u8 i;
	if(Setting.Setting_ReLoad_Mask)
	{
		Setting.Setting_ReLoad_Mask=0;
		if(Setting.Save_PassWord_Flag)
		{
			Setting.Save_PassWord_Flag = 0;
			Read_Save_SYS_PassWord(0);
		}
		if(Setting.Exit_Flag)
		{
			Setting.Mode=0;
			Setting.In_Flag=0;
			if(Setting.Save_PassWord_Flag == 0)
			if(Setting.PassWord_Changed_Flag)
			{
				Work_Status.SYS_Start_PassWord_EnAble = Work_Status.SYS_Start_PassWord_SBuf;
				Setting.PassWord_Changed_Flag = 0;
			}
			Config_Temp[5] = 0;
			Work_Status.Speed_Unit_GY_Flag 	= 	Config_Temp[0];
//			if(Speed_Limte_BUF != Config_Temp[8])
//			{
				if(Work_Status.Speed_Unit_GY_Flag)
						Speed_Limte_Code = SPEED_UNIT_CONVERT(0,Config_Temp[8])-10;
				else
						Speed_Limte_Code = Config_Temp[8] - 10;
//			}
			Work_Status.Power_Unit_PI_Flag  =   Config_Temp[7];
			Wheel_Dia_Mark					=   Config_Temp[6];
			SYS_P_V_Seclect					=	Config_Temp[4];
			Scenes_Mode_Flag 				= 	Config_Temp[3];
			SYS_Auto_OFF 					= 	Config_Temp[2];
			Work_Status.BLK_Light_Level 	= 	Config_Temp[1];
			AUTO_LIGHT_Sensitivity = Config_Temp[9];
			Read_Save_SYS_Setting_Parameter(0);
			if(AUTO_LIGHT_Sensitivity!=0)
			{
				Work_Status.Light_Sensing_EN = 1;
				if(AUTO_LIGHT_Sensitivity==5)
						LIGHT_ON_SET = AL_Standard_Value-160;
				else if(AUTO_LIGHT_Sensitivity==4)
						LIGHT_ON_SET = AL_Standard_Value-120;
				else if(AUTO_LIGHT_Sensitivity==3)
						LIGHT_ON_SET = AL_Standard_Value-80;
				else if(AUTO_LIGHT_Sensitivity==2)
						LIGHT_ON_SET = AL_Standard_Value-60;
				else if(AUTO_LIGHT_Sensitivity==1)
						LIGHT_ON_SET = AL_Standard_Value-40;
				LIGHT_OFF_SET = (LIGHT_ON_SET-20);
			}
			else
			{
				Work_Status.Light_Sensing_EN = 0;
			}
			if(Setting.Time_Need_To_Save_Flag)
			{
					Setting.Time_Need_To_Save_Flag = 0;
					Timer_Second = 0;
					RTC_Set_Time(Timer_Hour , Timer_Minute , Timer_Second);
			}
		}
		else
		{
			if(Setting.Mode==0)//一级菜单
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag=0;
					Dis_Play_Setting_BAck_Ground(Setting.View_S);
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					if(Setting.Chosen == 0)
					{
						if(Setting.View_S==0)
						{
							Setting.Card_ReLoad_Flag=0;
							Setting_Value_Temp=13-Setting.Selected;
							Setting_Value_Top_Limit=12;
							Setting_Value_Bottom_Limit=0;
						}
						else
						{
							Setting.Card_ReLoad_Flag=0;
							Setting_Value_Temp=3-Setting.Selected;
							Setting_Value_Top_Limit=2;
							Setting_Value_Bottom_Limit=0;
						}
					}
					else
					{
						switch(Setting.Chosen)
						{
							case 1:
							case 4:
								Setting_Value_Temp=Config_Temp[Setting.Chosen - 1];
								Setting_Value_Top_Limit=1;
								Setting_Value_Bottom_Limit=0;
							break;
							case 6:
							case 7:
								Setting_Value_Temp=Config_Temp[Setting.Chosen - 2];
								Setting_Value_Top_Limit=1;
								Setting_Value_Bottom_Limit=0;
							break;
							case 5:
								Setting_Value_Temp=Config_Temp[7];
								Setting_Value_Top_Limit=1;
								Setting_Value_Bottom_Limit=0;
							break;
							case 2:
								Setting_Value_Temp=Config_Temp[1];
								Setting_Value_Top_Limit=5;
								Setting_Value_Bottom_Limit=1;
							break;
							case 3:
								Setting_Value_Temp=Config_Temp[2];
								Setting_Value_Top_Limit=9;
								Setting_Value_Bottom_Limit=0;
							break;
							case 8://轮径参数
								Setting_Value_Temp=Config_Temp[6];
								Setting_Value_Top_Limit=34;
								Setting_Value_Bottom_Limit=6;
							break;
							case 9://限速参数
								Setting_Value_Temp=Config_Temp[8];
								if(Config_Temp[0])
									Setting_Value_Top_Limit=60;
								else
									Setting_Value_Top_Limit=90;
								Setting_Value_Bottom_Limit=10;
							break;
							case 10://光感灵敏度
								Setting_Value_Temp=Config_Temp[9];
								Setting_Value_Top_Limit=5;
								Setting_Value_Bottom_Limit=0;
							break;
						}
					}
				}
				if(Setting.Clrear_Trip_Flag)
				{
					Setting.Clrear_Trip_Flag = 0;
					Sport_Recording.Single_Trap_At_Once=0;
					Sport_Recording.Timer_Of_This_Trap=0;
					Read_Save_Single_Trap(0);
					Sport_Recording.MAX_Speed_In_This_Trap = 0;
					Read_Save_MAX_Trap(0);
					Sport_Recording.AVG_Speed_In_This_Trap = 0;
					Config_Temp[5] = 3;
				}
				if(Setting.Chosen == 0)
				{
					if(Setting.View_S==0)
						Setting.Selected=13-Setting_Value_Temp;
					else
						Setting.Selected=3-Setting_Value_Temp;
				}
				else
				{
					if(Setting.Chosen<5)
					{
						Config_Temp[Setting.Chosen - 1]=Setting_Value_Temp;
						if(Setting.Chosen == 1)
						{
							if(Config_Temp[0])
									Config_Temp[8] = Speed_Limte_MPH;
							else
									Config_Temp[8] = Speed_Limte_BUF;
						}
					}
					else if(Setting.Chosen == 5)
						Config_Temp[7]=Setting_Value_Temp;
					else if(Setting.Chosen == 6)
						Config_Temp[4]=Setting_Value_Temp;
					else
					{
						if(Setting.View_S==0)
						{
							if(Setting.Chosen == 7)//TRIP CLEAR
								Config_Temp[5]=Setting_Value_Temp;
							else if(Setting.Chosen == 8)//轮径
								Config_Temp[6]=Setting_Value_Temp;
							else if(Setting.Chosen == 9)//限速
								Config_Temp[8]=Setting_Value_Temp;
							else if(Setting.Chosen == 10)//光感灵敏度
								Config_Temp[9]=Setting_Value_Temp;
						}
					}
					if(Setting.Chosen == 2)
						Set_BK_Light_Leve(Config_Temp[1]);
				}
				Dis_Setting_View(Setting.View_S , (Setting.Selected + (Setting.Chosen<<4)) , Config_Temp);
			}
			else if(Setting.Mode==1)//二级菜单  密码界面
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag=0;
					Dis_Play_PassWord_Ground(0);
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					if(Setting.Chosen == 0)
					{
						if(Code_Input_Flag==0)
						{
							if(Work_Status.SYS_Start_PassWord_EnAble)
							{
									Setting_Value_Temp=2;
									Setting_Value_Top_Limit=2;
									Setting_Value_Bottom_Limit=0;
							}
							else
							{
									Setting_Value_Temp=1;
									Setting_Value_Top_Limit=1;
									Setting_Value_Bottom_Limit=0;
							}
						}
						else
						{
							Setting_Value_Temp=User_Input_Code_Tab[Code_Input_Flag-1];
							Setting_Value_Top_Limit=9;
							Setting_Value_Bottom_Limit=0;
						}
					}
					else
					{
						Setting_Value_Temp=Work_Status.SYS_Start_PassWord_EnAble;
						Setting_Value_Top_Limit=1;
						Setting_Value_Bottom_Limit=0;
					}
				}
				if(Setting.Chosen>0)
					Work_Status.SYS_Start_PassWord_EnAble = Setting_Value_Temp;
				else
				{
					if(Code_Input_Flag==0)
					{
						if(Work_Status.SYS_Start_PassWord_EnAble)
								Setting.Selected = 3-Setting_Value_Temp;
						else
								Setting.Selected = 2-Setting_Value_Temp;
					}
					else
						User_Input_Code_Tab[Code_Input_Flag-1] = Setting_Value_Temp;
				}
				if(Setting.PW_Set_MSG_Flag>0)
				{
					DisPlay_Full_Messages(Setting.PW_Set_MSG_Flag);
					if(Setting.Save_PassWord_Flag)
					{
						Setting.Save_PassWord_Flag = 0;
						Setting.PassWord_Changed_Flag = 0;
						Read_Save_SYS_PassWord(0);
					}
				}
				else
				{
					if(Setting.Password_Set_States)
							DisPlay_PassWord_InPut_View(Code_Input_Flag , User_Input_Code_Tab , Setting.Password_Set_States);
					else
							Dis_PassWord_Setting_View(Setting.Selected + (Setting.Chosen<<4) , Work_Status.SYS_Start_PassWord_EnAble);
				}
			}
			else if(Setting.Mode==3)//四级菜单  电池信息界面
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag=0;
					Page_S_BT_Buf = 8;
					Battery_BACK_Ground();
					DisPlay_Battery_Index(Bat_Sirl_Num , Bat_Index_Pages,Bat_Message,Bat_Valtages);
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					Setting_Value_Temp=1;
					Setting_Value_Top_Limit=1;
					Setting_Value_Bottom_Limit=0;
				}
				Setting.Selected = 2-Setting_Value_Temp;
				Battery_Index_View_Ground(Setting.Selected);
				if(Bat_Index_Page_Reload)
				{
					Bat_Index_Page_Reload = 0;
					DisPlay_Battery_Index(Bat_Sirl_Num , Bat_Index_Pages,Bat_Message,Bat_Valtages);
				}
			}
			else if(Setting.Mode==4)//五级菜单  错误代码页面
			{
				if(Setting.Error_Code_Clear_Flag)
				{
					Setting.Error_Code_Clear_Flag=0;
					for(i=0;i<11;i++)
						Error_Code_Tab[i]=0;
					S_Num_Err = 0;
					Read_Save_Err_CODE(0);
					Setting.Card_ReLoad_Flag=1;
				}
				if(Setting.UpDat_Flag)
				{
					Setting.Selected = 1;
					Setting.UpDat_Flag=0;
					Error_Memory_View_Ground();
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					Error_Code_Dis(Error_Code_Tab);
				}
			}
			else if(Setting.Mode==5)//六级菜单  时间设置
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag=0;
					Dis_Play_PassWord_Ground(3);
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					if(TIME_Set_Mask==0)
					{
						Setting_Value_Temp=Timer_Hour/10;
						Setting_Value_Top_Limit=2;
						Setting_Value_Bottom_Limit=0;
					}
					else if(TIME_Set_Mask==1)
					{
						Setting_Value_Temp=Timer_Hour%10;
						if(Timer_Hour/10 == 2)
							Setting_Value_Top_Limit=3;
						else
							Setting_Value_Top_Limit=9;
						Setting_Value_Bottom_Limit=0;
					}
					else if(TIME_Set_Mask==2)
					{
						Setting_Value_Temp=Timer_Minute/10;
						Setting_Value_Top_Limit=5;
						Setting_Value_Bottom_Limit=0;
					}
					else if(TIME_Set_Mask==3)
					{
						Setting_Value_Temp=Timer_Minute%10;
						Setting_Value_Top_Limit=9;
						Setting_Value_Bottom_Limit=0;
					}
					
				}
				if(TIME_Set_Mask==0)
				{
					Timer_Hour = Timer_Hour%10;
					Timer_Hour += Setting_Value_Temp*10;
				}
				else if(TIME_Set_Mask==1)
				{
					Timer_Hour /= 10;
					Timer_Hour *= 10;
					Timer_Hour += Setting_Value_Temp;
				}
				else if(TIME_Set_Mask==2)
				{
					Timer_Minute = Timer_Minute%10;
					Timer_Minute += Setting_Value_Temp*10;
				}
				else if(TIME_Set_Mask==3)
				{
					Timer_Minute /= 10;
					Timer_Minute *= 10;
					Timer_Minute += Setting_Value_Temp;
				}
				if(Timer_Hour>23)
					Timer_Hour = 23;
				if(Timer_Minute>59)
					Timer_Minute=59;
				DisPlay_Clock_SetView(TIME_Set_Mask , Timer_Hour , Timer_Minute);
			}
			else if(Setting.Mode==MY_SETTING_INDEX)//设置页面首页 index
			{
				if(Setting.UpDat_Flag)
				{
					Setting.UpDat_Flag=0;
					SETTING_INDEX_Page(0);
				}
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					Setting_Value_Temp=3-Setting.Selected;
					Setting_Value_Top_Limit=2;
					Setting_Value_Bottom_Limit=0;
				}
				Setting.Selected = 3-Setting_Value_Temp;
				SETTING_INDEX_Page(Setting.Selected);
			}
		}
	}
}
/* KEY ---->  >>按键处理程序*/
void Key_Mission_Process(u8 Key_Value9)
{
	u16 i;
	switch(Key_Value9)
	{
		case KEY_VALUE_i_S://短按 i  单击
			if(Setting.i_Key_IN == 0)
			{
				if(Setting.In_Flag==0)//非设置模式。切换档位
				{
					Work_Status.Interface_ReLoad_Mask=1;
					if(Work_Status.Working_Mode<6)
						Work_Status.Working_Mode++;
					else
						Work_Status.Working_Mode=0;
					if(Work_Status.Working_Mode == 4)
					{
							if(LCD_DisPlay.RANGE_DIS_EN == 0)
							{
									if(LCD_DisPlay.CALORIES_DIS_EN == 0)
										Work_Status.Working_Mode = 6;
									else
										Work_Status.Working_Mode = 5;
							}
					}
					else if(Work_Status.Working_Mode == 5)
					{
							if(LCD_DisPlay.CALORIES_DIS_EN == 0)
									Work_Status.Working_Mode = 6;
					}
				}
				else
				{
					if(Setting.Mode==0)//一级菜单，选定/取消选定项目
					{
						if(Setting.View_S == 0)//display setting
						{
							if(Setting.Selected==13)//返回主菜单
							{
								Setting.Mode=MY_SETTING_INDEX;
								Setting.Selected=1;
								Setting.Card_ReLoad_Flag = 1;
								Setting.UpDat_Flag = 1;
							}
							else if(Setting.Selected==11)//跳转至密码设置页面
							{
								Setting.Mode = 1;
								Setting.UpDat_Flag = 1;
								Setting.Card_ReLoad_Flag = 1;
								Work_Status.SYS_Start_PassWord_SBuf = Work_Status.SYS_Start_PassWord_EnAble;
								Setting.PassWord_Changed_Flag = 1;
								Setting.Password_Set_States = 0;
								Setting.PW_Set_MSG_Flag = 0;
								Setting.PW_Set_MSG_Delay = 0;
								Setting.Chosen = 0;
								Code_Input_Flag = 0;
								User_Input_Code_Tab[0]=10;
								User_Input_Code_Tab[1]=10;
								User_Input_Code_Tab[2]=10;
								User_Input_Code_Tab[3]=10;
							}
							else if(Setting.Selected==12)//跳转至 时钟设置界面
							{
								Setting.Mode = 5;
								Setting.UpDat_Flag = 1;
								Setting.Card_ReLoad_Flag = 1;
								Setting.Time_Need_To_Save_Flag = 1;
								TIME_Set_Mask=0;
							}
							else//进入深度设置模式
							{
								if(Setting.Chosen == 0)//选定项目
									Setting.Chosen = Setting.Selected;
								else
								{
									if(Setting.Selected == 7)
									{
										if(Config_Temp[5] == 1)//清零单次里程
										{
											Setting.Clrear_Trip_Flag = 1;
										}
									}
									if(Setting.Selected == 9)
									{
										if(Config_Temp[0])
										{
											Speed_Limte_MPH = Config_Temp[8];
											Speed_Limte_BUF = SPEED_UNIT_CONVERT(0,Config_Temp[8]);
										}
										else
										{
											Speed_Limte_MPH = SPEED_UNIT_CONVERT(1,Config_Temp[8]);
											Speed_Limte_BUF = Config_Temp[8];
										}
									}
									Setting.Chosen = 0;
								}
								Setting.Card_ReLoad_Flag=1;
							}
						}
						else if(Setting.View_S == 1)
						{
							if(Setting.Selected==3)//返回主菜单
							{
								Setting.Mode=MY_SETTING_INDEX;
								Setting.Selected=2;
								Setting.Card_ReLoad_Flag = 1;
								Setting.UpDat_Flag = 1;
							}
							else if(Setting.Selected==1)//跳转至电池信息界面
							{
								
								Battey_Index_Waiting = 2;
								Setting.Mode = 3;
								Setting.UpDat_Flag = 1;
								Setting.Card_ReLoad_Flag = 1;
							}
							else if(Setting.Selected==2)//跳转至错误代码记忆界面
							{
								Setting.Mode = 4;
								Setting.UpDat_Flag = 1;
								Setting.Card_ReLoad_Flag = 1;
							}
							
						}
					}
					else if(Setting.Mode==1)
					{
						if(Setting.Selected == 1)
						{
							Setting.Mode = 0;
							Setting.UpDat_Flag = 1;
							Setting.Card_ReLoad_Flag = 1;
							Setting.Selected = 11;
						}
						else if(Setting.Selected == 2)
						{
							if(Setting.Chosen == 0)
							{
								Setting.Chosen = Setting.Selected;
								Setting.PW_Last_States = Work_Status.SYS_Start_PassWord_EnAble;
							}
							else
							{
								Setting.Chosen = 0;
								if((Setting.PW_Last_States == 0) && (Work_Status.SYS_Start_PassWord_EnAble == 1))
								{
									Setting.Password_Set_States=2;
									Setting.Selected = 4;
									Code_Input_Flag = 1;
									Setting.UpDat_Flag = 1;
									for(i=0;i<4;i++)
											User_Input_Code_Tab[i]=10;
								}
								else if((Setting.PW_Last_States == 1) && (Work_Status.SYS_Start_PassWord_EnAble == 0))
								{
									Setting.Password_Set_States=6;
									Setting.Selected = 4;
									Code_Input_Flag = 1;
									Setting.UpDat_Flag = 1;
									for(i=0;i<4;i++)
											User_Input_Code_Tab[i]=10;
								}
							}
						}
						else if(Setting.Selected == 3)
						{
							Setting.Selected = 4;
							Code_Input_Flag = 1;
							Setting.Chosen = 0;
							Setting.Password_Set_States = 9;
							Setting.UpDat_Flag = 1;
							for(i=0;i<4;i++)
									User_Input_Code_Tab[i]=10;
						}
						else if(Setting.Selected == 4)//密码输入  模式
						{
							if(Code_Input_Flag<4)
							{
								if(User_Input_Code_Tab[Code_Input_Flag-1]<10)
									Code_Input_Flag++;
							}
							else if(User_Input_Code_Tab[Code_Input_Flag-1]<10)
							{
								if(Setting.Start_Code_Flag)
									Code_Input_Flag=5;
								else
								{
									switch(Setting.Password_Set_States)
									{
											case 2:
												Setting.Password_Set_States = 3;
												for(i=0;i<4;i++)
													User_Input_Code_Buf[i]=User_Input_Code_Tab[i];
												for(i=0;i<4;i++)
													User_Input_Code_Tab[i]=10;
												Code_Input_Flag = 1;
											break;
											case 3:
												if((User_Input_Code_Buf[0]==User_Input_Code_Tab[0])&&(User_Input_Code_Buf[1]==User_Input_Code_Tab[1])&&(User_Input_Code_Buf[2]==User_Input_Code_Tab[2])&&(User_Input_Code_Buf[3]==User_Input_Code_Tab[3]))
												{
													Setting.PW_Set_MSG_Flag = 1;//提示设置成功
													Setting.Selected = 1;
													Code_Input_Flag = 0;
													Setting.Password_Set_States=0;
													Setting.Save_PassWord_Flag = 1;
													for(i=0;i<4;i++)
														Sys_Code_Tab[i]=User_Input_Code_Tab[i];
												}
												else
												{
													Setting.Password_Set_States = 3;
													Setting.PW_Set_MSG_Flag = 2;//提示确认失败
													for(i=0;i<4;i++)
														User_Input_Code_Tab[i]=10;
													Code_Input_Flag = 1;
												}
											break;
											case 6:
												if((Sys_Code_Tab[0]==User_Input_Code_Tab[0])&&(Sys_Code_Tab[1]==User_Input_Code_Tab[1])&&(Sys_Code_Tab[2]==User_Input_Code_Tab[2])&&(Sys_Code_Tab[3]==User_Input_Code_Tab[3]))
												{
													Setting.PW_Set_MSG_Flag = 3;//提示设置成功
													Setting.Selected = 1;
													Code_Input_Flag = 0;
													Setting.Password_Set_States=0;
													Setting.Save_PassWord_Flag = 1;
												}
												else
												{
													Setting.PW_Set_MSG_Flag = 4;//提示失败
													Setting.Password_Set_States = 6;
													for(i=0;i<4;i++)
														User_Input_Code_Tab[i]=10;
													Code_Input_Flag = 1;
												}
											break;
											case 9:
												if((Sys_Code_Tab[0]==User_Input_Code_Tab[0])&&(Sys_Code_Tab[1]==User_Input_Code_Tab[1])&&(Sys_Code_Tab[2]==User_Input_Code_Tab[2])&&(Sys_Code_Tab[3]==User_Input_Code_Tab[3]))
												{
													Setting.Password_Set_States=10;
												}
												else
												{
													Setting.PW_Set_MSG_Flag = 4;//提示失败
													Setting.Password_Set_States = 9;
												}
												for(i=0;i<4;i++)
													User_Input_Code_Tab[i]=10;
												Code_Input_Flag = 1;
											break;
											case 10:
												Setting.Password_Set_States = 11;
												for(i=0;i<4;i++)
													User_Input_Code_Buf[i]=User_Input_Code_Tab[i];
												for(i=0;i<4;i++)
													User_Input_Code_Tab[i]=10;
												Code_Input_Flag = 1;
											break;
											case 11:
												if((User_Input_Code_Buf[0]==User_Input_Code_Tab[0])&&(User_Input_Code_Buf[1]==User_Input_Code_Tab[1])&&(User_Input_Code_Buf[2]==User_Input_Code_Tab[2])&&(User_Input_Code_Buf[3]==User_Input_Code_Tab[3]))
												{
													Setting.PW_Set_MSG_Flag = 5;//提示设置成功
													Setting.Selected = 1;
													Code_Input_Flag = 0;
													Setting.Password_Set_States=0;
													Setting.Save_PassWord_Flag = 1;
													for(i=0;i<4;i++)
														Sys_Code_Tab[i]=User_Input_Code_Tab[i];
												}
												else
												{
													Setting.Password_Set_States = 11;
													Setting.PW_Set_MSG_Flag = 2;//提示确认失败
													for(i=0;i<4;i++)
														User_Input_Code_Tab[i]=10;
													Code_Input_Flag = 1;
												}
											break;
									}
								}
							}
						}
						Setting.Card_ReLoad_Flag=1;
					}
					/*else if(Setting.Mode==2)
					{
						if(Setting.Selected == 1)//由恢复出厂设置页面返回
						{
							Setting.Mode = 0;
							Setting.UpDat_Flag = 1;
							Setting.Card_ReLoad_Flag = 1;
							Setting.Selected = 11;
						}
						else if(Setting.Selected == 2)
						{
							Setting.Restor_Now_Flag = 1;
						}
					}*/
					else if(Setting.Mode==3)//电池界面
					{
						if(Setting.Selected == 1)
						{
							Setting.Mode = 0;
							Battey_Index_Waiting = 0;
							Setting.UpDat_Flag = 1;
							Setting.Card_ReLoad_Flag = 1;
							Bat_Index_Page_Reload = 0;
							Bat_Index_Pages = 0;
							Setting.Selected = 1;
						}
						else if(Setting.Selected == 2)
						{
							Bat_Index_Page_Reload = 1;
							if(Bat_Index_Pages<3)
								Bat_Index_Pages++;
							else
								Bat_Index_Pages = 0;
						}
					}
					else if(Setting.Mode==4)
					{
						if(Setting.Selected == 1)
						{
							Setting.Mode = 0;
							Setting.UpDat_Flag = 1;
							Setting.Card_ReLoad_Flag = 1;
							Setting.Selected = 2;
						}
					}
					else if(Setting.Mode==5)
					{
						Setting.Card_ReLoad_Flag = 1;
						if(TIME_Set_Mask<3)
							TIME_Set_Mask++;
						else
						{
							Setting.Mode = 0;
							Setting.UpDat_Flag = 1;
							Setting.Selected = 12;
						}
					}
					else if(Setting.Mode==MY_SETTING_INDEX)
					{
						if(Setting.Selected == 3)//退出设置模式
						{
							Setting.Exit_Flag=1;
							Setting.Setting_ReLoad_Mask = 1;
						}
						else 
						{
							Setting.Mode = 0;
							Setting.UpDat_Flag = 1;
							Setting.Card_ReLoad_Flag = 1;
							if(Setting.Selected == 2)
							{
								Setting.View_S = 1;
							}
							else if(Setting.Selected == 1)
								Setting.View_S = 0;
							Setting.Selected = 1;
						}
					}
					Setting.Setting_ReLoad_Mask=1;
					Work_Status.SYS_Setting_OverFlow=0;
				}
			}
			else //i双击
			{
				Setting.i_Key_IN = 0;
//				if((Work_Status.Bike_Moving_Flag==0)&&(Work_Status.SYS_Err_Occurred_Flag==0))
				if(Work_Status.Bike_Moving_Flag==0)
				{
					if((Setting.In_Flag==0))//进入设置模式首页
					{
						Setting.In_Flag=1;
						Setting.Mode=MY_SETTING_INDEX;
						Setting.UpDat_Flag=1;
						Setting.Card_ReLoad_Flag=1;
						Setting.Setting_ReLoad_Mask=1;
						Setting.View_S = 0;
						Setting.Selected = 1;
						Setting.Chosen = 0;
						Bat_Index_Pages = 0;
						Setting.Restor_Now_Flag = 0;
						Setting.Save_PassWord_Flag = 0;
						Speed_Buf = 0;
						Config_Temp[0] = Work_Status.Speed_Unit_GY_Flag;
						Config_Temp[9] = AUTO_LIGHT_Sensitivity;
						Speed_Limte_MPH = SPEED_UNIT_CONVERT(1,Speed_Limte_Code + 10);
						if(Work_Status.Speed_Unit_GY_Flag)
								Config_Temp[8] = Speed_Limte_MPH;
						else
								Config_Temp[8] = Speed_Limte_Code + 10;
						Speed_Limte_BUF = Speed_Limte_Code + 10;
						Config_Temp[7] = Work_Status.Power_Unit_PI_Flag;
						Config_Temp[6] = Wheel_Dia_Mark;
						Config_Temp[4] = SYS_P_V_Seclect;
						Config_Temp[3] = Scenes_Mode_Flag;
						Config_Temp[2] = SYS_Auto_OFF;
						Config_Temp[1] = Work_Status.BLK_Light_Level;
					}
					else//退出设置，并保存参数
					{
						if(Work_Status.SYS_Err_Occurred_Flag!=0)
							LCD_DisPlay.ERR_ReLoad_Flag=1;
						Setting.Exit_Flag=1;
						Setting.Setting_ReLoad_Mask = 1;
					}
				}
			}
		break;
		case KEY_VALUE_UP_S://短按 up
			if(Setting.In_Flag==0)
			{
					if(PAS_Value<PAS_Top_Vf)//(Work_Status.SCA_Leve_Setting/2*2+3))
					{
						PAS_Value++;
						LCD_DisPlay.PAS_ReLoad_Flag=1;
						PAS_Send_Now_Mask=1;
//						if(Work_Status.Working_Mode==4 && PAS_Value==1)
//							Work_Status.Interface_ReLoad_Mask = 1;
					}
					else
					{
						PAS_Value_NG=1;
					}
			}
			else
			{
				if(Setting_Value_Temp<Setting_Value_Top_Limit)
            Setting_Value_Temp++;
        else
        {
            if(Setting_Value_Bottom_Limit>0)
                  Setting_Value_Temp=Setting_Value_Top_Limit;
            else
                  Setting_Value_Temp=Setting_Value_Bottom_Limit;
        }
				Setting.Setting_ReLoad_Mask=1;
				Work_Status.SYS_Setting_OverFlow=0;
			}
		break;
		case KEY_VALUE_DOWN_S://短按 down
			if(Setting.In_Flag==0)
			{
					if(PAS_Value>(Work_Status.SCA_Leve_Setting%2))
					{
						PAS_Value--;
						LCD_DisPlay.PAS_ReLoad_Flag=1;
						PAS_Send_Now_Mask=1;
//						if(Work_Status.Working_Mode==4 && PAS_Value==0)
//							Work_Status.Interface_ReLoad_Mask = 1;
					}
					else
					{
						PAS_Value_NG=1;
					}
			}
			else
			{
				if(Setting_Value_Temp>Setting_Value_Bottom_Limit)
            Setting_Value_Temp--;
        else
        {
            if(Setting_Value_Bottom_Limit>0)
                  Setting_Value_Temp=Setting_Value_Bottom_Limit;
            else
                  Setting_Value_Temp=Setting_Value_Top_Limit;
        }
				Setting.Setting_ReLoad_Mask=1;
				Work_Status.SYS_Setting_OverFlow=0;
			}
		break;	
		case (KEY_VALUE_UP_L)://长按 up  模式切换
			#ifndef POWER_YOUR_LIFE_LOGO_Words
			if(Setting.In_Flag==0)
			{
				if(Scenes_Mode_Flag)
					Scenes_Mode_Flag = 0;
				else
					Scenes_Mode_Flag = 1;
				LCD_DisPlay.E_S_SW_Flag = 1;
				MY_USART.Send_Scenes_Flag = 1;
			}
			#endif
		break;
		case KEY_VALUE_i_L://长按 i
			if(Setting.In_Flag!=0)
			{
				Setting.Setting_ReLoad_Mask = 1;
				Setting.Exit_Flag=1;
			}
		break;
//		case KEY_VALUE_DOWN_L://长按 DOWN  不保存设置，退出设置界面
//			if(Setting.In_Flag!=0 && Setting.Mode != 5 && Code_Input_Flag == 0)
//			{
//				Setting.Mode=0;
//				Setting.In_Flag=0;
//				Setting.Exit_Flag=1;
//				if(Work_Status.SYS_Err_Occurred_Flag)
//					LCD_DisPlay.ERR_ReLoad_Flag=1;
//			}
//		break;
		case (KEY_VALUE_DOWN_L | KEY_VALUE_UP_L | KEY_VALUE_PW_L)://UP DOWN POWER 三键长按 清除错误代码
			if(Setting.In_Flag!=0)
			{
				if(Setting.Mode==4)
				{
					Setting.Error_Code_Clear_Flag=1;
					Setting.Setting_ReLoad_Mask=1;
				}
			}
		break;
		case KEY_VALUE_LT_L://大灯 开 关  
			if(Night_Mode_Flag)
			{
				Night_Mode_Flag=0;
				Work_Status.Light_Switch_Flag=0;
			}
			else
			{
				Night_Mode_Flag=1;
				Work_Status.Light_Switch_Flag=1;
			}
			Work_Status.Light_Sensing_EN = 0;
			LCD_DisPlay.Light_ReLoad_Flag = 1;
			Send_Mode_S = 2;
			Send_Mode = 5;
			if(Work_Status.Light_Switch_Flag)
				Set_BK_Light_Leve(1);
			else
				Set_BK_Light_Leve(Work_Status.BLK_Light_Level);
//				Set_BK_Light_Leve(5);
		break;
//		case KEY_VALUE_PW_L://关机
//			if((!Work_Status.SYS_Power_Just_ON) && (EMC_TEST.EN_Flag == 0))
//			{
//				Work_Status.ShutDown_Flag = 1;
//			}
//		break;
		default:break;
	}
}
/*******
*定时中断处理子函数*-->[ 10mS ]<--
*******/
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	uint8_t  i,Key_Temp;
    if(htim->Instance == htim2.Instance)
    {
		if(Work_Status.Bike_Moving_Flag)//如果车子在动，则计算里程
		{
			if(++Mileage_Variable.Base_Time_Count_NUM > Mileage_Variable.Base_Time_Count_Delay)//基本时基单元（车轮转动一圈的时间）
			{
				Mileage_Variable.Base_Time_Count_NUM=0;
				Mileage_Variable.Base_mm_Count += Wheel_Perimeter;//时间到，里程增加一个车轮周长
				if(Mileage_Variable.Base_mm_Count>10000)//100m = 0.1Km  <1mS: 100000 = 100m>  <10mS: 10000 = 100m>
				{
					Mileage_Variable.Base_mm_Count-=10000;
					if(Sport_Recording.Total_Trap_Record<10000000)//最大可计数到 1000000.0Km
						Sport_Recording.Total_Trap_Record++;
					if(++Sport_Recording.Service_Milage_ADD>10)
					{
						Sport_Recording.Service_Milage_ADD = 0;
						if(Sport_Recording.Service_Milage<60000)
							Sport_Recording.Service_Milage++;
					}
					if(Sport_Recording.Single_Trap_At_Once<60000)//单次里程，每6000Km自动清零
						Sport_Recording.Single_Trap_At_Once++;
					else
					{
						Sport_Recording.Single_Trap_At_Once=0;
						Sport_Recording.Timer_Of_This_Trap=0;
					}
					if((Work_Status.Working_Mode == 0) || (Work_Status.Working_Mode == 1))
						Work_Status.Interface_ReLoad_Mask=1;//刷新显示
					if(++Trip_Record_Save_Delay>50)//每5km保存一次里程数据
					{
						Trip_Record_Save_Delay = 0;
						Work_Status.Save_Datas_Flag = 1;
					}
					Avg_JS_Flag = 1;
					Work_Status.Moving_State_Chg = 1;
				}
			}
			Sys_Auto_ShutUp_Delay = 0;
		}
		else
		{
			if(Work_Status.Moving_State_Chg)//车次由运动状态转变为静止后，保存一次数据
			{
				Work_Status.Moving_State_Chg = 0;
				Work_Status.Save_Datas_Flag = 1;
			}
			if((Motor_Power_CC < 10) && (EMC_TEST.EN_Flag == 0))
			{
				if(SYS_Auto_OFF > 0)
				{
					if(++Sys_Auto_ShutUp_Delay>5900 * SYS_Auto_OFF)//自动关机 计时   SYS_Auto_OFF
					{
						Work_Status.ShutDown_Flag = 1;
						if(Work_Status.USB_State_Flag)
							Work_Status.USB_Sleep_Flag = 1;
					}
				}
			}
			else
				Sys_Auto_ShutUp_Delay = 0;
			if(++AVG_Dec_Delay>150)
			{
				Avg_JS_Flag = 1;
				AVG_Dec_Delay = 0;
			}
		}
		
		
		
		if((!Work_Status.USB_Sleep_Flag)&& Work_Status.SYS_Power_States_Flag)
			Key_Temp = Key_Scan_Handler();
		else
			Key_Temp = 0;
		if(Work_Status.P_Walk_Flag)//Walk 模式下，按键值返回 0 则退出Walk模式
		{
			if((Key_Temp&0x02)==0)
			{
				if(++WALK_EXIT_DELAY>5)
				{
					WALK_EXIT_DELAY = 0;
					LCD_DisPlay.WALK_ReLoad_Flag = 1;
					Work_Status.P_Walk_Flag=0;
					PAS_Send_Now_Mask = 1;
				}
			}
			else
				WALK_EXIT_DELAY = 0;
		}
		if(Key_Temp&0x80)//存在有效按键
		{
			Sys_Auto_ShutUp_Delay=0;//清除自动关机 计时
			if(Key_Temp==0xc2  && (Setting.In_Flag==0))
			{
				LCD_DisPlay.WALK_ReLoad_Flag = 1;
				Work_Status.P_Walk_Flag=1;
				PAS_Send_Now_Mask=1;
			}
			if(Key_Temp == KEY_VALUE_i_S && Work_Status.SYS_Power_States_Flag)
			{
				if(Setting.i_Key_DL>0)
				{
					Setting.i_Key_IN = 1;
					Setting.i_Key_ON = 0;
					Setting.i_Key_DL = 0;
					Key_Mission_Process(Key_Temp);
				}
				else
				{
					Setting.i_Key_ON = 1;
					Setting.i_Key_DL = 28;//0.28秒
					Setting.i_Key_IN = 0;
				}
			}
			else
				Key_Mission_Process(Key_Temp);
		}
		if(Work_Status.SYS_Power_Just_ON)
		{
			if(Key_Temp==0 && Work_Status.SYS_Power_States_Flag)
				Work_Status.SYS_Power_Just_ON=0;
		}
		else
		{
			if(!Key_MOD)
			{
					if(++Shut_SYS_Delay>180)
						Work_Status.ShutDown_Flag = 1;
			}
			else
				Shut_SYS_Delay = 0;
		}
		if(MY_USART.RevBack_Time_Out>0)
		{
			MY_USART.RevBack_Time_Out--;
			if(MY_USART.RevBack_Time_Out==0)
			{
				MY_USART.RevBack_Time_Out_Flag=1;
			}
		}
		if(MY_USART.Just_Send_Flag)
		{
			if(++MY_USART.Send_Time_Delay>5)
			{
				MY_USART.Send_Time_Delay = 0;
				MY_USART.Just_Send_Flag = 0;
			}
		}
		else
			MY_USART.Send_Time_Delay = 0;
		if(Setting.In_Flag)
		{
			if(UART_Send_Settings_Flag==0)
			{
				if(++Send_Uart_Delay>100)
				{
					Send_Uart_Delay = 0;
					UART_Send_Settings_Flag=1;
				}
			}
			else
				Send_Uart_Delay = 0;
		}
		if(Setting.i_Key_ON)
		{
			if(Setting.i_Key_DL>0)
				Setting.i_Key_DL--;
			else
			{
				Setting.i_Key_DL = 0;
				Setting.i_Key_ON = 0;
				Setting.i_Key_IN = 0;
				Key_Mission_Process(KEY_VALUE_i_S);
			}
		}
		/*串口接收结束判断----------------------------------------------------***  -USART Recive END -  ***--------                                                      */
		Rx_Cnt = USART_Get_NUMS();
		if(Rx_Cnt>0)
		{
			if(Rx_Cnt != Rx_Cnt_Last)
			{
					Rx_Cnt_Last = Rx_Cnt;
					Rx_Time_Over = 0;
			}
			else
			{
					if(++Rx_Time_Over>USART1_RXTime_OUT)
					{
						Rx_Time_Over = 0;
						HAL_UART_DMAStop(&huart1);
						Rx_Len = USART_Get_NUMS();
						RX_NUM = Rx_Len;
						for(i=0;i<Rx_Len;i++)
							Rx_Buf[i]=Rx_DMA_Buf[i];
						RX_Flag = 1;
						HAL_UART_Receive_DMA(&huart1, (uint8_t *)Rx_DMA_Buf , 128);
					}
			}
			
		}
		else
		{
			Rx_Time_Over = 0;
			Rx_Cnt_Last = 0;
		}
		/*走时函数-------------------------------------------------------------*****   - Timer -  *****     ---------                                                       */
		if(++Timer_Second_Delay>97)
		{
			Timer_Second_Delay = 0;
//			LCD_DisPlay.TIME_ReLoad_Flag = 1;
			if(Timer_Second < 59)
				Timer_Second++;
			else
			{
				Timer_Second = 0;
				if(Timer_Minute<59)
				{
					Timer_Minute++;
					if(Time_For_BF_ShutWithClear<9000)
						Time_For_BF_ShutWithClear++;
					if(++Sport_Recording.Timer_Of_This_Trap > 50000)
					{
						Sport_Recording.Timer_Of_This_Trap = 0;
						Sport_Recording.Single_Trap_At_Once = 0;
					}
					if(Work_Status.Working_Mode==6)
						Work_Status.Interface_ReLoad_Mask = 1;
				}
				else
				{
					Timer_Minute = 0;
					if(Timer_Hour<23)
						Timer_Hour++;
					else
						Timer_Hour=0;
				}
				LCD_DisPlay.TIME_ReLoad_Flag = 1;
			}
		}
		if(++D500mS_Count>50)
		{
			D500mS_Count = 0;
			Work_Status.See_Volatge_Flag = 1;
			if(Setting.In_Flag && !Setting.Start_Code_Flag)//设置模式下，超过1min无操作，自动退出
			{
				if(++Work_Status.SYS_Setting_OverFlow>40)
				{
					Work_Status.SYS_Setting_OverFlow=0;
					Setting.Mode=0;
					Setting.Chosen = 0;
					Setting.In_Flag=0;
					Setting.Exit_Flag=1;
					Work_Status.Call_IN_From_PC_Flag=0;
					if(Work_Status.SYS_Err_Occurred_Flag)
						LCD_DisPlay.ERR_ReLoad_Flag=1;
				}
			}
			if(Work_Status.SYS_Start_ON_Flag)
			{
				if(++MY_USART.REV_Back_OUT_Time>TIME_OF_USART_RESET)//3S
				{
						MY_USART.REV_Back_OUT_Time = 0;
						UART1_DeInit();
						MX_USART1_UART_Init(1200);
						Int_Uart_DMA_Receive();//开启串口接收
				}
			}
			else
				MY_USART.REV_Back_OUT_Time = 0;
			if((Work_Status.Current_Err_CODE != 0x30)&&(Setting.In_Flag==0)&&(Work_Status.SYS_Start_ON_Flag))
			{
				if((++Work_Status.NoCommunication_OverFlow>NoCommunication_OverStand) && (Work_Status.Engineering_Mode_MASK!=1))//通讯超时   检测
				{
					LCD_DisPlay.ERR_ReLoad_Flag = 1;
					Work_Status.NoCommunication_OverFlow=0;
					Work_Status.Current_Err_CODE=0x30;
//					Setting.SOC_Rev_Flag = 0;// 通讯超时，无SOC，用电压显示
					Battery_SOC = 0;
					Battery_SOC_FR = Battery_SOC;
					Work_Status.Current_Speed=0;//通讯失败，速度、功率清零
					Motor_Power_CC_Br=0;
					Motor_Power_CC=0;
					LCD_DisPlay.Bat_ReLoad_Flag = 1;
					LCD_DisPlay.Spd_ReLoad_Flag=1;
					LCD_DisPlay.PCC_ReLoad_Flag=1;
					Work_Status.Bike_Moving_Flag=0;
					Mileage_Variable.Base_Time_Count_Delay=60000;
					D_Speed=0;
				}
			}
			if((LCD_DisPlay.R_S_Flag) && (Setting.In_Flag==0))
			{
					if(++LCD_DisPlay.R_DIS_SDelay>40)//超过20S
					{
							LCD_DisPlay.R_DIS_SDelay = 0;
							LCD_DisPlay.R_S_Flag = 0;
							LCD_DisPlay.RANGE_DIS_EN = 0;//关闭剩余里程显示
					}
			}
			if((LCD_DisPlay.C_S_Flag) && (Setting.In_Flag==0))
			{
					if(++LCD_DisPlay.C_DIS_SDelay>40)//超过20S
					{
							LCD_DisPlay.C_DIS_SDelay = 0;
							LCD_DisPlay.C_S_Flag = 0;
							LCD_DisPlay.CALORIES_DIS_EN = 0;//关闭卡路里显示
					}
			}
			if(Setting.PW_Set_MSG_Flag>0)
			{
					if(++Setting.PW_Set_MSG_Delay>3)
					{
							Setting.PW_Set_MSG_Flag = 0;
							Setting.PW_Set_MSG_Delay = 0;
							Setting.UpDat_Flag = 1;
							Setting.Setting_ReLoad_Mask=1;
							Work_Status.SYS_Setting_OverFlow=0;
					}
			}
		}
		
		if(++Speed_ReLoad_Delay>5)//速度渐变刷新  SPD_buf 
		{
			Speed_ReLoad_Delay=0;
			if((Speed_Buf>0) || (D_Speed>0))
				LCD_DisPlay.Spd_ReLoad_Flag=1;
		}
		
		if(USB_Ts)
		{
			if(Work_Status.USB_State_Flag)
			{
				if(++USB_Check_Delay>50)
				{
					USB_Check_Delay = 0;
					Work_Status.USB_State_Flag = 0;
					LCD_DisPlay.USB_ReLoad_Flag = 1;
				}
			}
			USB_Relase_Delay = 0;
		}
		else
		{
			if(!Work_Status.USB_State_Flag)
			{
				if(++USB_Relase_Delay>50)
				{
					USB_Relase_Delay = 0;
					Work_Status.USB_State_Flag = 1;
					LCD_DisPlay.USB_ReLoad_Flag = 1;
				}
			}
			USB_Check_Delay = 0;
		}
		
		if(PAS_NG_Delay>0)
		{
			PAS_NG_Delay--;
			if(PAS_NG_Delay == 0)
				PAS_Value_NNG=1;
		}
		if((EMC_TEST.EN_Flag == 0) && (EMC_TEST.Start_Delay>0))//EMC监测 3分钟计时
		{
			EMC_TEST.Start_Delay--;
			EMC_TEST.EMC_Check_OK = 1;
			if(Work_Status.Current_Speed>150)//如果速度大于15km/h
			{
				if(EMC_TEST.EMC_Sped_Buf > Work_Status.Current_Speed)
				{
					if((EMC_TEST.EMC_Sped_Buf - Work_Status.Current_Speed)>5)
						EMC_TEST.EMC_Check_OK = 0;
				}
				else if(EMC_TEST.EMC_Sped_Buf > Work_Status.Current_Speed)
				{
					if((Work_Status.Current_Speed - EMC_TEST.EMC_Sped_Buf)>5)
						EMC_TEST.EMC_Check_OK = 0;
				}
				EMC_TEST.EMC_Sped_Buf = Work_Status.Current_Speed;
			}
			else
				EMC_TEST.EMC_Check_OK = 0;
			if(PAS_Value<3)
				EMC_TEST.EMC_Check_OK = 0;
			if(Moto_Working_Flag == 0)
				EMC_TEST.EMC_Check_OK = 0;
			if(C_Current>8)
				EMC_TEST.EMC_Check_OK = 0;
			if(EMC_TEST.EMC_Check_OK)
			{
				if(++EMC_TEST.Check_Delay>1500)
				{
					EMC_TEST.EN_Flag = 1;//启动EMC模式
					EMC_TEST.Check_Delay = 0;
					EMC_TEST.EMC_Sped_Buf = Work_Status.Current_Speed;//锁定当前速度
					NoCommunication_OverStand = Err_30_Time_Delay_E;
				}
			}
			else
			{
				EMC_TEST.Check_Delay = 0;
			}
		}
		if(EMC_TEST.EN_Flag)
		{
			if((Key_Temp&0x80) && (Key_Temp != 0xC1) && (Key_Temp != 0x81))//存在有效按键
//			if((Key_Temp&0x80) && (Key_Temp != 0x81))//存在有效按键
			{
				EMC_TEST.EN_Flag = 0;
				EMC_TEST.Start_Delay = 18000;//重启EMC监测
				NoCommunication_OverStand = Err_30_Time_Delay;
				EMC_TEST.Check_Delay = 0;
			}
			if(C_Current>9)
			{
				if(++EMC_TEST.EMC_Exit_Delay>300)
				{
					EMC_TEST.EMC_Exit_Delay = 0;
					EMC_TEST.EN_Flag = 0;
					EMC_TEST.Start_Delay = 18000;//重启EMC监测
					NoCommunication_OverStand = Err_30_Time_Delay;
					EMC_TEST.Check_Delay = 0;
				}
			}
			else
				EMC_TEST.EMC_Exit_Delay = 0;
		}
	}
}
/*开机密码输入界面*/
void DisPlay_PassWord_InView(void)
{
	unsigned char P_s = 0 , Ds_Delay=0 , i , GJ_Delays = 0 , GJ_EN = 0;
	u16 CODE_IN_TIME_OVER=0;
	PAS_Value = 0;	
	Send_PAS_Value();
	Work_Status.USB_Sleep_Flag = 0;
	Work_Status.SYS_Power_States_Flag = 1;
	Setting.Start_Code_Flag = 1;
	User_Input_Code_Tab[0]=10;
	User_Input_Code_Tab[1]=10;
	User_Input_Code_Tab[2]=10;
	User_Input_Code_Tab[3]=10;
	Setting.In_Flag = 1;
	Setting.Mode = 1;
	Setting.Selected = 4;
	Code_Input_Flag = 1;
	Setting.Setting_ReLoad_Mask = 1;
	Setting.Card_ReLoad_Flag = 1;
	LCD_Init();
//	#ifdef EV_BIKE_LOGO
		LCD_Clear(Black);
//	#else
//		LCD_Clear(Green);
//	#endif
	Dis_Play_PassWord_Ground(1);
	DisPlay_PassWord_InPut_View(Code_Input_Flag , User_Input_Code_Tab , 0);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	Set_BK_Light_Leve(Work_Status.BLK_Light_Level);//打开背光
	while(1)//等待正确密码
	{
		if(Code_Input_Flag<5)
		{
			if(Setting.Setting_ReLoad_Mask)
			{
				if(Setting.Card_ReLoad_Flag)
				{
					Setting.Card_ReLoad_Flag=0;
					Setting_Value_Bottom_Limit=0;
					Setting_Value_Temp=User_Input_Code_Tab[Code_Input_Flag-1];
					Setting_Value_Top_Limit=9;
				}
				User_Input_Code_Tab[Code_Input_Flag-1]=Setting_Value_Temp;
				DisPlay_PassWord_InPut_View(Code_Input_Flag , User_Input_Code_Tab , 0);
				Setting.Setting_ReLoad_Mask=0;
				CODE_IN_TIME_OVER = 0;
			}
		}
		else
		{
			if((User_Input_Code_Tab[0]==Sys_Code_Tab[0])&&(User_Input_Code_Tab[1]==Sys_Code_Tab[1])\
				&&(User_Input_Code_Tab[2]==Sys_Code_Tab[2])&&(User_Input_Code_Tab[3]==Sys_Code_Tab[3]))
			{
				Setting.Mode = 0;
				Setting.In_Flag = 0;
				Code_Input_Flag = 0;
				Setting.Selected = 0;
				PAS_Value = DEFAULT_START_PAS;
				Setting.Start_Code_Flag = 0;
				
				Sys_Auto_ShutUp_Delay = 0;
				Work_Status.ShutDown_Flag = 0;//清零自动关机
				break;
			}
			else
			{
				Code_Input_Flag = 1;
				DisPlay_PassWord_InPut_View(Code_Input_Flag , User_Input_Code_Tab , 0);
				if(++P_s>2)
				{
					Show_Word_Agency(10,320,0,"Bye-bye~              ",Yellow,Black);
					Show_Word_Agency(10,285,0,"                      ",Red,Black);
					delay_ms(800);
					Set_BK_Light_Leve(0);
					LCD_Clear(Black);
					Work_Status.SYS_Power_States_Flag=0;
					HAL_TIM_Base_Stop(&htim2);//关闭定时器
					HAL_TIM_Base_Stop(&htim3);
					delay_ms(5);
					System_Power_OFF;
				}
				else
				{
					Show_Word_Agency(10,320,0,"Password Incorrect!",Red,Black);
					if(P_s == 1)
						Show_Word_Agency(10,285,0,"Remaining -2- times",Magenta,Black);
					else if(P_s == 2)
						Show_Word_Agency(10,285,0,"Remaining -1- time  ",Magenta,Black);
					for(i=0;i<4;i++)
						User_Input_Code_Tab[i]=10;
				}
			}
		}
		if(++Ds_Delay>50)
		{
			PAS_Value = 0;	
			Send_PAS_Value();
		}
		if(!Key_MOD)
		{
			if(GJ_EN)
			{
				if(++GJ_Delays>180)
				{
					Sys_ShutUP_I();
				}
			}
		}
		else
		{
			GJ_Delays = 0;
			GJ_EN = 1;
		}
		if(++CODE_IN_TIME_OVER>6000)
			System_Power_OFF;
		delay_ms(10);
	}
	Work_Status.SYS_Power_States_Flag = 0;
}
/*缓存仪表型号、软硬件版本号，配置校验码*/
void Set_BF_Ver_Datas(void)
{
	u8 i,Sum_Check;
	
	Sum_Check = 0;
	BF_SWHW_Ver_Tab[0] = 0x10;
	BF_SWHW_Ver_Tab[1] = 13;
	Sum_Check = 29;
	for(i=0;i<13;i++)
	{
		BF_SWHW_Ver_Tab[i+2]=BF_DP_SWHW_Ver[i];
		Sum_Check += (u8)BF_DP_SWHW_Ver[i];
	}
	BF_SWHW_Ver_Tab[15] = Sum_Check;
	
	Sum_Check = 0;
	BF_HW_Ver_Tab[0]=0X17;
	BF_HW_Ver_Tab[1]=12;
	Sum_Check = 35;
	for(i=0;i<12;i++)
	{
		BF_HW_Ver_Tab[i+2]=BF_DP_HW_Ver[i];
		Sum_Check += (u8)BF_DP_HW_Ver[i];
	}
	BF_HW_Ver_Tab[14]=Sum_Check;
	
	Sum_Check = 0;
	BF_Model_Tab_Temp[0]=0X11;
	BF_Model_Tab_Temp[1]=0X0A;
	BF_Model_Tab_Temp[2]=0X0B;
	Sum_Check = 0x26;
	for(i=0;i<11;i++)
	{
		BF_Model_Tab_Temp[i+3]=BF_DP_Model[i];
		Sum_Check += (u8)BF_DP_Model[i];
	}
	BF_Model_Tab_Temp[14]=Sum_Check;
	
	Read_Save_Product_SN(1);
	delay_ms(5);
	Read_Save_Frame_SN(1);
	delay_ms(5);
	Read_Save_Gust_Sirl(1);
}
/*判断是否连接PC*/
void ACK_Connect_PC(void)
{
	uint8_t 	i , PC_Wait , ConnectWait=0 , PC_ACK_Flag=0 , DIS_State=0 , X_Lens=0;
	uint16_t	KeyM_Fliter_Delay=0;
	
	PC_Wait = 4;
	while(PC_Wait)
	{
		Rx_Time_Over = 0;
		Rx_Cnt = 0;
		RX_Flag = 0;
		Call_PC_Code();
		ConnectWait = 0;
		while((RX_Flag==0) && (ConnectWait<200))
		{
			ConnectWait++;
			delay_ms(1);
		}
		if(RX_Flag)
		{
			RX_Flag=0;
			if((Rx_Buf[0]==0x90)&&(Rx_Buf[1]==0x40)&&(Rx_Buf[2]==0xd0))
			{
				PC_ACK_Flag=0;//控制器连接，进入主程序
				break;
			}
			if((Rx_Buf[0]==0x90)&&(Rx_Buf[1]==0xff)&&(Rx_Buf[2]==0x8f))
			{
				PC_ACK_Flag=1;//PC连接
				break;
			}
		}
		PC_Wait--;
	}
	if(PC_ACK_Flag)
	{
		
		USART1_RXTime_OUT = 1;
		System_Power_ON;//系统上电
//		Txt_y = 375;
		MX_USART1_UART_Init(9600);
		Int_Uart_DMA_Receive();//开启串口接收
		LCD_Init();
		LCD_Clear(Black);
		Show_Word_Agency(40,380,0,"Connected OK",White,Black);
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
		Set_BK_Light_Leve(2);
		Set_BF_Ver_Datas();
		delay_ms(2);
		while(!Key_MOD);//等待开机键松开
		Work_Status.SYS_Power_Just_ON = 0;
		while(1)
		{
			if(RX_Flag)
			{
				RX_Flag=0;
//				Show_Word_Agency(40,140,0,"RX IN",Green,Black);
				if((Rx_Buf[0]==0x11)&&(Rx_Len==3))
				{
					if((Rx_Buf[1]==0x0A) && (Rx_Buf[2]==0x1B))//获取仪表型号
					{
						USART_Send_Data((uint8_t *)&BF_Model_Tab_Temp,15);
						if(DIS_State!=1)
						{
							Show_Word_Agency(40,340,0,"Read OK ",Green,Black);
							DIS_State=1;
						}
					}
					else if((Rx_Buf[1]==0x0B) && (Rx_Buf[2]==0x1C))//获取客户号
					{
//						Read_Save_Gust_Sirl(1);
						Send_PC_Gust();
						if(DIS_State!=1)
						{
							Show_Word_Agency(40,340,0,"Read OK ",Green,Black);
							DIS_State=1;
						}
					}
					if((Rx_Buf[1]==0x0D) && (Rx_Buf[2]==0x1E))//获取参数
					{
						Send_Beest_Para();
						if(DIS_State!=1)
						{
							Show_Word_Agency(40,340,0,"Read OK ",Green,Black);
							DIS_State=1;
						}
					}
				}
				else if((Rx_Buf[0]==0x12)&&(Rx_Len==2))
				{
					if(Rx_Buf[1]==0x10)//读取软件版本号
					{
						USART_Send_Data((uint8_t *)&BF_SWHW_Ver_Tab,16);
						if(DIS_State!=1)
						{
							Show_Word_Agency(40,340,0,"Read OK ",Green,Black);
							DIS_State=1;
						}
					}
					else if(Rx_Buf[1]==0x14)//读生产序列号
					{
//						Read_Save_Product_SN(1);
						Send_PC_Product_SN();
						if(DIS_State!=1)
						{
							Show_Word_Agency(40,340,0,"Read OK ",Green,Black);
							DIS_State=1;
						}
					}
					else if(Rx_Buf[1]==0x16)//读售后里程
					{
						Send_PC_Sevice_Milage();
						if(DIS_State!=1)
						{
							Show_Word_Agency(40,340,0,"Read OK ",Green,Black);
							DIS_State=1;
						}
					}
					else if(Rx_Buf[1]==0x11)//读车架号
					{
//						Read_Save_Frame_SN(1);
						Send_PC_Frame_SN();
						if(DIS_State!=1)
						{
							Show_Word_Agency(40,340,0,"Read OK ",Green,Black);
							DIS_State=1;
						}
					}
					else if(Rx_Buf[1]==0x17)//读硬件版本号
					{
						USART_Send_Data((uint8_t *)&BF_HW_Ver_Tab,15);
						if(DIS_State!=1)
						{
							Show_Word_Agency(40,340,0,"Read OK ",Green,Black);
							DIS_State=1;
						}
					}
				}
				else if(Rx_Buf[0]==0x16)
				{
					if(Rx_Buf[1]==0X0B)//设置客户号
					{
						USART_Send_Data(Rx_Buf,Rx_Len);
						X_Lens = Rx_Buf[2];
						if(X_Lens>25)
						{
							X_Lens = 25;
							Rx_Buf[2]=25;
						}
						for(i=0;i<(X_Lens+1);i++)
							BF_Gust_Sirl[i]=Rx_Buf[2+i];
						Read_Save_Gust_Sirl(0);
//						Send_PC_Gust();
						if(DIS_State!=2)
						{
							Show_Word_Agency(40,340,0,"Write OK ",Green,Black);
							DIS_State=2;
						}
					}
					else if(Rx_Buf[1]==0X0D)//设置参数
					{
						USART_Send_Data(Rx_Buf,Rx_Len);
						Wheel_Dia_Mark = Rx_Buf[4];
						Speed_Limte_Code = Rx_Buf[5]-10;
						AT24CXX_WriteOneByte(8,Wheel_Dia_Mark);delay_ms(5);
						AT24CXX_WriteOneByte(10,Speed_Limte_Code);
//						Send_Beest_Para();
						if(DIS_State!=2)
						{
							Show_Word_Agency(40,340,0,"Write OK ",Green,Black);
							DIS_State=2;
						}
					}
				}
				else if(Rx_Buf[0]==0x18)
				{
					if(Rx_Buf[1]==0x01)//写生产序列号
					{
						USART_Send_Data(Rx_Buf,Rx_Len);
						X_Lens = Rx_Buf[2];
						if(X_Lens>40)
						{
							X_Lens = 40;
							Rx_Buf[2]=40;
						}
						for(i=0;i<(X_Lens+1);i++)
							BF_Product_SN[i]=Rx_Buf[2+i];
						Read_Save_Product_SN(0);
						if(DIS_State!=2)
						{
							Show_Word_Agency(40,340,0,"Write OK ",Green,Black);
							DIS_State=2;
						}
					}
					if(Rx_Buf[1]==0x08)//写车架号
					{
						USART_Send_Data(Rx_Buf,Rx_Len);
						X_Lens = Rx_Buf[2];
						if(X_Lens>15)
						{
							X_Lens = 15;
							Rx_Buf[2]=15;
						}
						for(i=0;i<(X_Lens+1);i++)
							BF_Frame_SN[i]=Rx_Buf[2+i];
						Read_Save_Frame_SN(0);
						if(DIS_State!=2)
						{
							Show_Word_Agency(40,340,0,"Write OK ",Green,Black);
							DIS_State=2;
						}
					}
					else if(Rx_Buf[1]==0x11 && Rx_Buf[2]==0x02)//清除售后里程
					{
						USART_Send_Data(Rx_Buf,Rx_Len);
						Sport_Recording.Service_Milage = 0;
						Read_Save_Single_Trap(0);
						if(DIS_State!=3)
						{
							Show_Word_Agency(40,340,0,"Clear OK ",Green,Black);
							DIS_State=3;
						}
					}
					else if((Rx_Buf[1]==0x10)&&(Rx_Buf[2]==0x28)&&(Rx_Len==3))//退出 beest 进入正常工作状态
					{
						USART_Send_Data(Rx_Buf,Rx_Len);
						break;
					}
				}
			}
//			if(Txt_y<40)
//			{
//				Txt_y = 375;
//				LCD_Clear(Black);
//				Dis_LOGO(1);
//				Show_Word_Agency(10,410,0,"PC Connected...",White,Black);
//			}
			if(!Key_MOD)
			{
				if(++KeyM_Fliter_Delay>1500)
					Sys_ShutUP_I();
				delay_ms(1);
			}
			else
				KeyM_Fliter_Delay = 0;
		}
		USART1_RXTime_OUT = 2;
		MX_USART1_UART_Init(1200);//改变串口波特率
		Int_Uart_DMA_Receive();//开启串口接收
		Work_Status.SYS_Power_Just_ON=1;
	}
}
/* USER CODE END 0 */
int main(void)
{
  /* USER CODE BEGIN 1 */
	uint8_t i = 0;
//	u8 Ddd[7];
	uint32_t P_s;
	SCB->VTOR = FLASH_BASE | 0x10000;
	Start_MCU();
	
	Send_Data[0] = 0x11;
	Send_Data[1] = 0x11;
	USART_Send_Data(Send_Data,2);
	delay_ms(30);
	USART_Send_Data(Send_Data,2);
	
	LCD_Init();
//	Set_BK_Light_Leve(0);
	SaveAndRead_Data_By_aBigTab(0x87);//读取数据
	S_Num_Err = Error_Code_Tab[10];
	delay_ms(5);
	Initialization_System_Start();//系统参数初始化
	delay_ms(5);
	ACK_Connect_PC();
	System_Power_ON;//系统上电
	if(Work_Status.SYS_Start_PassWord_EnAble)/*开机密码*/
		DisPlay_PassWord_InView();
//	#ifdef EV_BIKE_LOGO
//		LCD_Clear(Black);
//	#else
		LCD_Clear(Def_BGC);
//	#endif
	delay_ms(50);
	MX_TIM3_Init();
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	Set_BK_Light_Leve(Work_Status.BLK_Light_Level);//打开背光
	if(LOGO_Dis_Flag)
	{
		Dis_LOGO(0);
		Work_Status.Engineering_Mode_MASK = 0;
	}
	else
	{
		Dis_LOGO(1);
		Work_Status.Engineering_Mode_MASK = 1;
	}
	
	delay_ms(100);
	Send_Scenes_Mode();
	delay_ms(100);
	Send_PAS_Value();
	
	while(ADC_Value[147] == 0);
	P_s = 0;
	for(i=0;i<50;i++)
		P_s += ADC_Value[3*i];
	SYS_Voltage_Start = P_s/V_Standard_Value;
	SYS_Voltage = SYS_Voltage_Start;
	
	EMC_TEST.Start_Delay = 18000;
	EMC_TEST.EN_Flag = 0;
	EMC_TEST.Check_Delay = 0;
//	Send_Mode_TopL = 5;
	Send_Mode = 3;
	MY_USART.Need_Rev_Data_Mask = 0;
	Int_Index_DisPlay();/*页面显示初始化*/
	Work_Status.SYS_Start_ON_Flag = 1;
	Work_Status.SYS_Power_States_Flag = 1;
	Battery_SOC_Start = 1;
	
	
	
  while (1)
  {
		if(RX_Flag)
		{
			RX_Flag=0;
			if(Setting.In_Flag==0)
			{
					J_DisPlay_Receive_CallBack();
			}
			else
			{
				if((Battey_Index_Waiting==2) && (Rx_Len==27))//处理接收到的电池信息1
				{
					if(Rx_Buf[0]!=0)
					{
						for(i=0;i<27;i++)
							Bat_Message[i]=Rx_Buf[i];
						Setting.Setting_ReLoad_Mask = 1;
						Bat_Index_Page_Reload = 1;
						Battey_Index_Waiting--;
					}
				}
				else if(Battey_Index_Waiting==1)//处理接收到的电池信息2 （电压）
				{
					if(Rx_Buf[0]<20 && Rx_Buf[0]>8)
					{
						Bat_Sirl_Num = Rx_Buf[0];
						for(i=0;i<(Rx_Buf[0]*2);i++)
							Bat_Valtages[i]=Rx_Buf[i+1];
						Setting.Setting_ReLoad_Mask = 1;
						Bat_Index_Page_Reload = 1;
						Battey_Index_Waiting=0;
					}
				}
			}
		}
		if(Setting.In_Flag==0)
		{
			LCD_DisPlay_ReLoad();
			if(MY_USART.Just_Send_Flag==0)
			{
					DisPlay_Send_Running_Data();
			}
		}
		else
		{
			Setting_Menu_List();
			if(UART_Send_Settings_Flag)
			{
				UART_Send_Settings_Flag = 0;
				if(Battey_Index_Waiting == 2)
				{
					for(i=0;i<27;i++)
						Bat_Message[i]=0;
					Read_battey_Index(1);
				}
				else if(Battey_Index_Waiting == 1)
				{
					for(i=0;i<40;i++)
							Bat_Valtages[i]=Rx_Buf[i+1];
					Read_battey_Index(0);
				}
			}
		}
		
		if(Work_Status.See_Volatge_Flag)
		{
			Work_Status.See_Volatge_Flag = 0;
			P_s = 0;
			for(i=0;i<50;i++)
				P_s += ADC_Value[3*i];
			SYS_Voltage_Start = (P_s/V_Standard_Value) + 3;
			if(SYS_Voltage != SYS_Voltage_Start)
			{
				SYS_Voltage = SYS_Voltage_Start;
				LCD_DisPlay.Bat_ReLoad_Flag = 1;
			}
			if(Work_Status.Light_Sensing_EN)//光感使能与否
			{
				P_s = 0;
				for(i=0;i<50;i++)
					P_s += ADC_Value[2 + (3*i)];
				P_s/=50;
				if(Work_Status.Light_Sensing_Flag)
				{
					if(P_s < LIGHT_OFF_SET)
					{
						if(++Work_Status.Light_Sense_Filter>2)
						{
							Work_Status.Light_Sensing_Flag = 0;
							Night_Mode_Flag=0;
							Work_Status.Light_Switch_Flag=0;
							LCD_DisPlay.Light_ReLoad_Flag = 1;
							Send_Mode_S = 2;
							Send_Mode = 5;
							Set_BK_Light_Leve(Work_Status.BLK_Light_Level);
						}
					}
					else
						Work_Status.Light_Sense_Filter = 0;
				}
				else
				{
					if(P_s > LIGHT_ON_SET)
					{
						if(++Work_Status.Light_Sense_Filter>2)
						{
							Work_Status.Light_Sensing_Flag = 1;
							Night_Mode_Flag=1;
							Work_Status.Light_Switch_Flag=1;
							LCD_DisPlay.Light_ReLoad_Flag = 1;
							Send_Mode_S = 2;
							Send_Mode = 5;
							Set_BK_Light_Leve(1);
						}
					}
					else
						Work_Status.Light_Sense_Filter = 0;
				}
			}
		}
		
		if(Work_Status.ShutDown_Flag)
		{
			Work_Status.ShutDown_Flag = 0;
			if(Work_Status.USB_Sleep_Flag)//充电状态下休眠
				Sys_Sleep_USB();
			else
				Sys_ShutUP_I();
		}
		
		if(Work_Status.Save_Datas_Flag)
		{
			Work_Status.Save_Datas_Flag = 0;
			Read_Save_Total_Trap_Record(0);//保存数据
			Read_Save_Single_Trap(0);
			if(Work_Status.MAX_SPD_Reset)
			{
				Work_Status.MAX_SPD_Reset=0;
				Read_Save_MAX_Trap(0);
			}
		}
		
		if(PAS_Send_Now_Mask==1)
		{
				PAS_Send_Now_Mask=0;
				Send_Mode_S = 7;
				Send_Mode = 5;
		}
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE|RCC_OSCILLATORTYPE_LSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.LSEState = RCC_LSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 240;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
/**
  * @brief  Early Wakeup WWDG callback.
  * @param  hwwdg: pointer to a WWDG_HandleTypeDef structure that contains
  *              the configuration information for the specified WWDG module.
  * @retval None
  */
void HAL_WWDG_WakeupCallback(WWDG_HandleTypeDef* hwwdg)
{
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_WWDG_WakeupCallback could be implemented in the user file
   */
    /*##-2- Refresh the WWDG #####################################################*/
    HAL_WWDG_Refresh(hwwdg, 127);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
