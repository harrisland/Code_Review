/**
  ******************************************************************************
  * File Name          : USART.h
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  *
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usart_H
#define __usart_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f2xx_hal.h"
#include "delay.h"
#define TIME_OF_REV_OUT 			15		//接收等待超时  150mS
#define TIME_OF_USART_RESET		6			//串口超过X秒复位时间 时间=X/2
#define Err_30_Time_Delay		 	12		//串口通讯超时，ERR30报错时间 5S
#define Err_30_Time_Delay_E		120		
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern UART_HandleTypeDef huart1;

/* USER CODE BEGIN Private defines */
extern uint8_t Rx_Buf[128];
//extern uint8_t Rx_DMA_Buf[64];
extern uint8_t Rx_Cnt , Rx_Cnt_Last;
extern uint8_t Rx_Time_Over;
extern uint8_t Rx_DMA_Buf[128];
//extern uint8_t recv_end_flag;

/* USER CODE END Private defines */

extern void Error_Handler(void);
void UART1_DeInit(void);
void MX_USART1_UART_Init(uint32_t BaudR);

/* USER CODE BEGIN Prototypes */
void Send_Bate_Data(u8 Da0);

void USART_Send_Data(uint8_t *Dat , u8 Len);
uint8_t USART_Get_NUMS(void);
void Int_Uart_DMA_Receive(void);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ usart_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
