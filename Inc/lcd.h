#ifndef __LED_H
#define __LED_H
#include "delay.h"
#include "usart.h"
//#include "Config.h"




//LCD端口定义

#define LCD_RST_1 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9 , GPIO_PIN_SET)
#define LCD_RST_0 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9 , GPIO_PIN_RESET)

#define LCD_RS_1 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8 , GPIO_PIN_SET)
#define LCD_RS_0 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8 , GPIO_PIN_RESET)

#define LCD_RD_1 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7 , GPIO_PIN_SET)
#define LCD_RD_0 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7 , GPIO_PIN_RESET)

#define LCD_WR_1 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6 , GPIO_PIN_SET)
#define LCD_WR_0 HAL_GPIO_WritePin(GPIOC, GPIO_PIN_6 , GPIO_PIN_RESET)
//#define LCD_RST PCout(9)
//#define LCD_RS  PCout(8)
//#define LCD_WR  PCout(6)
//#define LCD_RD  PCout(6)

#define Blue		0XC2C0//0XDD89
#define WBlue		0XDDAB//0XC2C0
#define WBlues		0XF013//0XF4C0
#define DBlue       0xEF12
#define DBlues      0xEA9D//0xEF12//0xF7CB
#define White		0X0000
#define White1		0X39E7
#define GWhite		0XCE79
#define Grey		0XF79E//灰色

#define Black       0xffff
#define BlackK      0xDEFB
#define NBlack      0xe71c
#define WGrey       0x8c71//0x4a69//0xef7c//灰白色  仅用于刻度 ef7d
#define Blue2       0xEF18//0xEF18   C610
#define Red         0x375C
#define Reds        0x375C//0x7FDE
#define WRed        0x15F7
#define DRed        0x15F7
#define WReds       0x15F7
#define Magenta     0x02bd
#define Green       0xF81F
#define WGreen       0xF17c
#define NotWhite    0xf7be
#define Cyan        0xe71c
#define Cyans        0xf800
#define Yellow      0x001f
//#define NotYellow   0xffe0
#define Orange      0X155D
extern uint8_t Page_S_BT_Buf;
extern uint8_t Night_Mode_Flag;
extern uint8_t Scenes_Mode_Flag;
extern uint8_t SYS_Unit_S369_Flag;

void LCD_WriteReg(uint16_t Command);
void LCD_WriteData(uint16_t Data);
void LCD_Reset(void);
void LCD_Init(void);
void LCD_SetXY(uint16_t x,uint16_t y,uint16_t w);
void LCD_Clear(uint16_t color);
void LCD_Fill_Back_Ground(uint8_t unit_flag , uint8_t PI_flag , uint8_t MODE_DIS_EN);
void DisPlay_ECO_SPORT_SWITCH(uint8_t unit_flag , uint8_t PI_flag , uint8_t S_EN);
void DiaPlay_USB(uint8_t Sta);
void Draw_Circle_Line(uint8_t yx , uint16_t r , uint16_t V_Angle , uint16_t Cloro);
void Draw_Speed_Arc(uint16_t Spdi);//显示速度的圆弧
//void Dis_LOGO(uint8_t Sf);
void Show_DD_Syb(uint8_t State);
void Dis_A128_NumS(uint8_t X_Pos,uint8_t Dat0);
void Dis_A64_Num(uint8_t Da0);
//void Dis_The_Big_Point(uint8_t States,uint16_t P_x);
void Dis_The_Big_Point(uint8_t States);
void Dis_Bat_Fream(void);
void Dis_Power_Left_Continue(uint16_t SPL);
void Dis_Valtage_Nums(uint8_t State , uint8_t B_SOC , uint16_t V_Sys);
void DisPlay_Real_Time(uint8_t Hour , uint8_t Minute);
//void DisPlay_HMS_Time(uint8_t Hour , uint8_t Minute , uint8_t Seconds);
void Clr_TFTLCD_Area(uint16_t x0 , uint16_t y0 , uint16_t x1 , uint16_t y1 , uint16_t Clor);
void Draw_XLine(uint16_t x0 , uint16_t x1 , uint16_t y0 , uint16_t Cloro);//画横线
void Dis_Play_ASS(uint8_t As_N,uint8_t State);/*显示档位值*/
void DisPlay_Char(uint8_t Num1 , uint16_t x0 , uint16_t y0 , uint16_t F_Cloro , uint16_t B_Cloro);
void Dis_MODE_NumS(uint32_t Nums , uint8_t Point_Flag, uint8_t wc);
void Dis_Play_Walk(uint8_t State);/*显示Walk图标 1-显示 0-不显示*/
//void Dis_Play_Informations(uint16_t A_Sped,uint16_t M_Sped,uint16_t T_Mage,uint32_t O_Mage,uint16_t T_TIM);
void Dis_Play_POWER_CC(uint8_t P_CIn);/*显示POWER功率值*/
void Dis_Play_Speeds(uint16_t SPD0);
void Dis_Play_Setting_BAck_Ground(u8 View_Sel);/*设置界面背景*/
void Fill_Word_Space(uint16_t X_i,uint16_t Y_j,uint8_t FX_State);
uint16_t Show_Word_Agency(uint16_t X_i,uint16_t Y_j,uint8_t FX_State,uint8_t *Str , uint16_t F_Color , uint16_t B_Color);
void Show_Word_DUnits(uint8_t Sn);
void Dis_A20_NumS(uint16_t X_i , uint16_t Y_j , uint8_t *Str , uint16_t F_Color, uint16_t B_Color);/*在指定位置显示高度为20的数字串*/
void Dis_Setting_View(uint8_t View_S , uint8_t M_Selected , uint8_t *Config_Tab);
void Dis_HEX_Num(uint16_t x , uint16_t y , uint16_t SCloro , uint16_t BCloro , uint16_t Da0);
void DisPlay_Err_Message(uint8_t Err_Code);
void Enable_Basic_Edit_View(uint8_t M_Selected);
void DisPlay_Clock_SetView(u8 Cl_Sel,u8 H_Num,u8 M_Num);
void Dis_Make_Sure_View(uint8_t M_Selected);
void Dis_Play_PassWord_Ground(uint8_t States);
void Battery_Index_View_Ground(uint8_t M_Selected);
void Battery_BACK_Ground(void);
void Error_Memory_View_Ground(void);
void Error_Code_Dis(uint8_t *Error_Codes);
void DisPlay_Battery_Index(uint8_t BT_S , uint8_t Page_S , uint8_t *Bat_Index_Tab , uint8_t *Bat_Val_Tab);
void Dis_a_SetNumber(uint16_t x , uint16_t y , uint8_t Point_EN , uint16_t SCloro , uint16_t BCloro , uint16_t Da0);
void Dis_PassWord_Setting_View(uint8_t M_Selected , uint8_t Sta);
void DisPlay_PassWord_InPut_View(uint8_t S_Sel , uint8_t *Pas_W , u8 Meg_Dis);
void DisPlay_Full_Messages(u8 Msg0);
void SETTING_INDEX_Page(u8 I_Sel);


#endif
